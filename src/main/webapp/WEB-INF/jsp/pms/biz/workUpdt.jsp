<%--
  Class Name : workUpdt.jsp
  Description : 작업상세조회, 수정 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>작업 상세 및 수정</title>

<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	gfn_init();

	$( "#workStartDt,#workEndDt" ).datepicker({
      showOn: "button",
      buttonImage: "/pmis/images/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Select date",
      dateFormat: "yy-mm-dd"
    });
	
	
	/// 1.콤보초기화 및 세팅
	//사업구분 콤보 공통코드
	gfn_setCombo("bizCat", "BIZN", "${bizDtl.bizCat}","", function(){
		
		//사업명 콤보
		gfn_setCbOption($("#bizSeq")
			, {	
				sqlId : "selectBizMngList2",
				sCd : "bizSeq", 
				sNm : "bizNm",
				bizCat : "${bizDtl.bizCat}",
				defaultVal : "${bizDtl.bizSeq}",
			} 
			, function(){
				
				// 공정명 콤보
				gfn_setCbOption($("#schedSeq"), {	
					sqlId : "selectSchedList2",
					sCd : "schedSeq", 
					sNm : "schedNm",
					bizSeq : "${bizDtl.bizSeq}",
					defaultVal : "${bizDtl.schedSeq}",
				});
		});
	});
	
	// 공정구분 콤보 공통코드
	gfn_setCombo("schedCat", "PROC");
	
	
	// 사업구분 변경이벤트
	$("#bizCat").on("change", function(d){
		
		//사업명 재조회
		gfn_setCbOption($("#bizSeq"), {	
			sqlId : "selectBizMngList2",
			sCd : "bizSeq", 
			sNm : "bizNm",
			bizCat : $(this).val(),
			//defaultVal : "",
		});
		
	});
	
	// 사업마스터 변경이벤트
	$("#bizSeq").on("change", function(d){
		
		//공정명 재조회
		gfn_setCbOption($("#schedSeq"), {	
			sqlId : "selectSchedList2",
			sCd : "schedSeq", 
			sNm : "schedNm",
			bizSeq : $(this).val(),
			//defaultVal : "",
		});
	});

	
	
	
	
	// 버튼권한처리
	setBtnAuth();	
	if('${mode}' == 'MOD'){
		$("select").attr("disabled",true);
	}
	
});









function fnListPage(){
    //document.frm.action = "<c:url value='/pms/biz/workMngList.do'/>";
    //document.frm.submit();
	location.href = '/pmis/pms/biz/workMngList.do';
}

function fnRefresh(){
    //$("input[name='bizSeq']").val($("#bizSeq").val());
    //$("input[name='schedSeq']").val($("#schedSeq").val());
	/*     
	    document.frm.action = "<c:url value='/pms/biz/workUpdt.do'/>";
	    document.frm.bizSeq.value = $("#bizSeq").val();
	    document.frm.schedSeq.value = $("#schedSeq").val();
	    document.frm.workSeq.value = $("input[name='workSeq']").val();
	    document.frm.mode.value = "${mode}";
	    document.frm.submit();
	 */
	location.reload();
}




/* 
function fnDelete(checkedIds) {
    if(confirm("<spring:message code="common.delete.msg" />")){
        document.frm.action = "<c:url value='/pms/biz/deleteWork.do'/>";
        document.frm.submit(); 
    }
}
 */
 function fnDelete(){
		
		if(!confirm("삭제 하시겠습니까?")) return;

		// 셀렉트박스 폼데이터 전송을위해서 임시로 disabled 해제
		$("select").attr("disabled",false);

		gfn_saveForm("frm", "/pmis/pms/biz/deleteWork.do" , function(data){
			
			if(data.result){
				alert("정상적으로 삭제되었습니다.");
				fnListPage();
			}else{
				alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
			}
		});
	}

/*  
function fnUpdate(){
    //if(validateUserManageVO(document.bizModVO)){
    //}
    
    if(!gfn_formValid("frm"))	return;

	if(!confirm("저장하시겠습니까?")) return;
    
    document.frm.bizSeq.value=$("#bizSeq").val();
    document.frm.schedSeq.value=$("#schedSeq").val();
    document.frm.workSeq.value=$("input[name='workSeq']").val();
    $("input[name='bizSeq']").val($("#bizSeq").val());
    $("input[name='schedSeq']").val($("#schedSeq").val());
 	document.frm.submit();
}
 */
function fnUpdate(){
    
    if(!gfn_formValid("frm"))	return;
    
	if(!confirm("저장하시겠습니까?")) return;
	
	// 셀렉트박스 폼데이터 전송을위해서 임시로 disabled 해제
	$("select").attr("disabled",false);
	
	gfn_saveFormCmm("frm", "updateWork", function(data){
		if(data.result){
			alert("데이터가 저장되었습니다.");
			if("${mode}" == "ADD"){
				fnListPage();
			}
			else{
				fnRefresh();
			}
		}else{
			alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
		}
	});	
	
}
 

 
//확인처리
function fnAprv(){

    if(!confirm("작업을 승인하시겠습니까?"))	return;
    
	gfn_saveCmm({sqlId:"updateWorkAprv", data:{bizSeq: "${bizDtl.bizSeq}", schedSeq: "${bizDtl.schedSeq}", workSeq: "${bizDtl.workSeq}", }  }
	,function(data){
		if (data.result) {
			alert("승인 되었습니다.");
			fnRefresh();
			return;
		}		
		else{
			alert("저장에 실패하였습니다.","E");
			return;
		}
	});
}




/// 버튼및권한처리
var setBtnAuth = function(){

	switch("${bizDtl.aprvYn}"){
		
		case "Y" : //승인
			// 승인버튼 
			$("#btnAprv").attr("disabled",true);
			$("#btnSave").attr("disabled",true);
			$("#btnDel").attr("disabled",true);
			
			break;
			
		case "N" : //미승인		
			// 승인버튼 - 관리자만 수정가능 
			if("${isBizMngYn}" == "Y"  || "${isAdmMngYn}" == "Y" ){
				$("#btnAprv").attr("disabled",false);
			}
			else{
				$("#btnAprv").attr("disabled",true);
			}
			
			//본인,관리자만 수정가능
			if("${isBizMngYn}" == "Y" || "${sessionScope.id}" == "${bizDtl.regId}" || "${isAdmMngYn}" == "Y" ){
				$("#btnSave").attr("disabled",false);
				$("#btnDel").attr("disabled",false);
			}
			else{
				$("#btnSave").attr("disabled",true);
				$("#btnDel").attr("disabled",true);
			}
		
			break;
			
		default :	
			break;
	};
	
	
}


//-->
</script>

</head>
<body>
<noscript>자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>    

<!-- container -->
<div id="container"  class="content" > 
  
  <!-- content -->
  <div id="content" > 
    
    <!--contents-box-->
    
    <div class="contents-box"  > 
      
      <!--conts-->
      <div class="conts"  >
        <h1><i class="fa fa-chevron-circle-right  "></i> 작업관리 </h1>
        
       	<!-- write -->
       	<form id="frm" name="frm">
	        <!-- 상세정보 작업 삭제시 prameter 전달용 input -->
	        <input name="checkedIdForDel" type="hidden" />
	        
			<!-- 	        검색조건 유지 -->
	        <input type="hidden" name="searchCondition" value="<c:out value='${bizDtl.searchCondition}'/>"/>
	        <input type="hidden" name="searchKeyword" value="<c:out value='${bizDtl.searchKeyword}'/>"/>
	        <input type="hidden" name="pageIndex" value="<c:out value='${bizDtl.pageIndex}'/>"/>
	        
			<input type="hidden" name="workSeq" value="<c:out value='${bizDtl.workSeq}'/>"/> 
            <input type="hidden" name="mode" value="<c:out value='${bizDtl.mode}'/>"/>
	        
	        <div class="table-responsive">
	          <table class="table table-write">
	            <caption>
	            	작업관리
	            </caption>
	            <colgroup>
	            <col width="20%">
	            <col width="80%">
	            </colgroup>
	            <tbody>
	              <tr>
	                <th>사업구분</th>
	                <td>
	                    <select class="form-control input-inline" style="width:25%"  name="bizCat" id="bizCat"     >
	                    </select>
	                  </td>
	              </tr>
	              <tr>
	                <th>사업명</th>
	                <td>
	                    <select class="form-control input-inline" style="width:25%"  name="bizSeq" id="bizSeq"  >
	                    </select>
	                </td>
	              </tr>
	               <tr>
	                 <th>공정명</th>
	                 <td>
	                    <select class="form-control input-inline" style="width:25%"  name="schedSeq" id="schedSeq" > 
	                    </select>
	                 </td>
	               </tr>
	               <tr>
	                 <th>공정구분</th>
	                 <td>
	                    <select class="form-control input-inline" style="width:25%"  name="schedCat" id="schedCat">
	                    </select>
	                 </td>
	               </tr>
	               <tr>
	                 <th class="req">제목</th>
	                 <td>
	                    <input name="ttl" id="ttl" class="form-control reqVal"  size="20"  maxlength="60" value="${bizDtl.ttl}" />
               		 </td>
	               </tr>
	              <tr>
	                <th>내용</th>
	                <td>
	                    <textarea name="contents" id="contents" class="form-control" style="height:200px;" rows="60" >${bizDtl.contents}</textarea>
	                </td>
	              </tr>
	             
	              <tr>
	                <th>투입기간</th>
	                <td>
	                    <input name="workStartDt" id="workStartDt" class="form-control" style="width:150px;" size="20"  maxlength="60" value="${bizDtl.workStartDt}"/> &nbsp;~&nbsp;
	                    <input name="workEndDt" id="workEndDt" class="form-control"  style="width:150px;" size="20"  maxlength="60" value="${bizDtl.workEndDt}" />
                  	</td>
	              </tr>
	              <tr>
	                <th class="num">투입공수</th>
	                <td>
	                    <input name="runMpw" id="runMpw" class="form-control numVal" style="width:150px;" value="${bizDtl.runMpw}"  />
                  	</td>
	              </tr>
	            </tbody>
	          </table>
	        </div>
	        
	        
	        <div class="btn_group right "  >
            	<button id="btnAprv" class="btn btn-default" onclick="JavaScript:fnAprv(); return false;"> <i class="fa fa-circle-o" aria-hidden="true"></i> 확인</button>
	            <button id="btnDel" class="btn btn-default " onclick="fnDelete(); return false;" ><i class="fa fa-close" aria-hidden="true"></i> 삭제</button>
	          	<button id="btnSave" class="btn btn-default" onclick="JavaScript:fnUpdate(); return false;" > <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
	           	<button id="button" class="btn btn-default" onclick="JavaScript:fnListPage(); return false;" > <i class="fa fa-reply" aria-hidden="true"></i> 취소</button>
	        </div>
        

        </form>
        <!-- //write --> 
        
      </div>
      
      <!--conts--> 
      
    </div>
    
    <!--//contents-box--> 
    
  </div>
  <!-- //content --> 
</div>
<!-- //container --> 

</body>
</html>

