<%--
  Class Name : docUpdt.jsp
  Description : 작업상세조회, 수정 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>작업 상세 및 수정</title>

<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<link href="<c:url value='/css/fileStyle.css' />" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<c:url value='/js/EgovMultiFile.js'/>"></script> 
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	
	gfn_init();
	
	if('${mode}' == 'MOD'){
		$("select").attr("disabled",true);
	}

	// 첨부 가능여부 처리 
	fn_egov_check_file("Y");
	//------------------------------------------
	//------------------------- 첨부파일 수정 Start
	//-------------------------------------------
	var maxFileNum = document.getElementById('posblAtchFileNumber').value;
	if(maxFileNum==null || maxFileNum==""){ maxFileNum = 3;}
	
	var multi_selector = new MultiSelector( document.getElementById( 'egovComFileList' ), maxFileNum, 'file_label');
	multi_selector.addElement( document.getElementById( 'egovfile_1' ) );
	if(!gfn_isNull("${bizDtl.fileSeq}")){
		fn_egov_multi_selector_update_setting(multi_selector);
	}	
	//------------------------- 첨부파일 수정 End
	
	
	// 버튼권한처리
	setBtnAuth();	
	
	
	// 메시지 처리 
	var sResultMsg = '<c:if test="${!empty resultMsg}"><spring:message code="${resultMsg}" /></c:if>';
	if(sResultMsg.length != 0 ) {
		alert(sResultMsg);
	}
	
});

function fnListPage(){
	location.href = '/pmis/pms/biz/docMngList.do';
    //document.bizModVO.action = "<c:url value='/pms/biz/docMngList.do'/>";
    //document.bizModVO.submit();
}
 
function fnRefresh(){
	location.reload();
    //document.bizModVO.action = "<c:url value='/pms/biz/docUpdt.do'/>";
    //document.bizModVO.submit();
}

// 승인처리
function fnAprv(){

    if(!confirm("본사업을 승인하시겠습니까?"))	return;
    
	gfn_saveCmm({sqlId:"updateBizAprv", data:{bizSeq: "${bizDtl.bizSeq}"}  }
	,function(data){
		if (data.result) {
			alert("승인 되었습니다.");
			fnRefresh();
			return;
		}		
		else{
			alert("저장에 실패하였습니다.","E");
			return;
		}
	});
}



// 산출물파일저장
function fnFileUpdate(nSeq){
	
	    document.bizModVO.bizSeq.value = nSeq;
	    document.bizModVO.mode.value = "UPDATE";
	    document.bizModVO.action = "<c:url value='/pms/biz/docFileUpdate.do'/>";
	    document.bizModVO.submit();		
}



/// 버튼및권한처리
var setBtnAuth = function(){
	switch("${bizDtl.aprvYn}"){
		
		case "Y" : //승인
		
			// 승인버튼 
			$("#btnAprv").attr("disabled",true);
			// 파일저장
			$("#btnSave").attr("disabled",true);
			
			//파일삭제
			//$("img[src='/pmis/images/btn/icon_file_delete.png']").prop("onclick",false);
			
			
			break;
			
		case "N" : //미승인		
			// 승인버튼 - 관리자만 수정가능 
			if("${isBizMngYn}" == "Y" ){
				$("#btnAprv").attr("disabled",false);
			}
			else{
				$("#btnAprv").attr("disabled",true);
			}
			// 파일저장
			$("#btnSave").attr("disabled",false);
		
			//파일삭제
			//$("img[src='/pmis/images/btn/icon_file_delete.png']").prop("onclick",true);
			break;
			
		default :	
			break;
	};
	
	
}




function fn_egov_check_file(flag) {
    if (flag=="Y") {
        document.getElementById('file_upload_posbl').style.display = "block";
        document.getElementById('file_upload_imposbl').style.display = "none";          
    } else {
        document.getElementById('file_upload_posbl').style.display = "none";
        document.getElementById('file_upload_imposbl').style.display = "block";
    }
}




//-->
</script>

</head>
<body>
<noscript>자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>    

        
        <div class="conts"  >
         	<h1><i class="fa fa-chevron-circle-right  "></i> 산출물관리</h1>

    		<form:form commandName="bizModVO" action="" name="bizModVO" method="post" enctype="multipart/form-data"  >
		       	<input name="bizSeq" type="hidden" id="bizSeq" value="${bizModVO.bizSeq}" />
		       	<input name="mode" type="hidden" id="mode" value="${bizModVO.mode}" />
				<!-- 최대 첨부파일 갯수  -->
		       	<input name="posblAtchFileNumber" type="hidden" id="posblAtchFileNumber" value="3" />
          
          <!-- write -->
          <div class="table-responsive">
            <table class="table table-view">
              <caption>산출물관리</caption>
              <colgroup>
              <col width="15%">
              <col width="85%">
              </colgroup>
              <tbody>
                <tr>
                  <th>사업구분</th>
                  <td>${bizDtl.bizCatNm}</td>
                </tr>
                <tr>
                  <th>사업명</th>
                  <td>${bizDtl.bizNm}</td>
                </tr>
                 <tr>
                   <th>사업기간</th>
                   <td>${bizDtl.bizTerm}</td>
                 </tr>
                 <tr>
                   <th>개요</th>
                   <td>${bizDtl.bizDesc}</td>
                 </tr>
                 <tr>
                   <th class="num">사업비용</th>
                   <td class="numVal left" >${bizDtl.bizCost} KRW</td>
                 </tr>
                <tr>
                  <th>공정계획</th>
                  <td>
                     <!--table list -->
                   <table class="table table-in">
                <colgroup>
                <col width="8%">
                <col width="12%">
                <col width="15%">
                <col width="12%">
                <col width="12%">
                <col width="8%">
                <col width="8%">
                <col width="8%">
                <col width="8%">
                <col width="8%">
                </colgroup>
                <thead>
                  <tr >
                    <th rowspan="2">순번</th>
                    <th rowspan="2">공정구분</th>
                    <th rowspan="2">공정명</th>
                    <th colspan="2">사업기간</th>
                    <th colspan="2">진행율(%)</th>
                    <th colspan="3">투입공수(M/M)</th>
                    </tr>
                  <tr>
                    <th>시작일자</th>
                    <th>종료일자</th>
                    <th>계획</th>
                    <th>진행</th>
                    <th>계획</th>
                    <th>실적</th>
                    <th>비율(%)</th>
                    </tr>
                </thead>
                <tbody>
				<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td class="lt_text3" colspan="9">
							<spring:message code="common.nodata.msg" />
						</td>
					</tr>
				</c:if>
				<c:forEach items="${resultList}" var="result" varStatus="status">
                  <tr <c:if test="${result.schedCatNm == '합계'}"> class="total" </c:if> >
                  	<c:choose >
                  		<c:when test="${result.schedCatNm == '합계'}">
                    		<td colspan="5" class="total">합계</td>
                  		</c:when>
                  		<c:otherwise>
		                    <td>${status.count}</td>
		                    <td >${result.schedCatNm}</td>
		                    <td>${result.schedNm}</td>
		                    <td>${result.schedStartDt}</td>
		                    <td>${result.schedEndDt}</td>
                  		</c:otherwise>
                  	</c:choose>
                  	
                    <td>${result.planRate}</td>
                    <td>${result.runRate}</td>
                    <td>${result.planMpw}</td>
                    <td>${result.runMpw}</td>
                    <td>${result.mpwRate}</td>
                  </tr>
				</c:forEach>

                 

                </tbody>
              </table>
                  <!--//table list -->
                  
                  </td>
                </tr>
                
				<tr>
                   <th>산출물</th>
                   <td >
						<c:if test="${bizModVO.fileSeq ne null && bizModVO.fileSeq ne ''}">
							<c:import charEncoding="utf-8" url="/cmm/fms/selectFileInfsForUpdateFileSeq.do" >
							<c:param name="param_fileSeq" value="${bizModVO.fileSeq}" />
							<c:param name="returnUrl" value="/pms/biz/docUpdt.do" />
							</c:import>
						</c:if>
			  			
			  			 <!-- 첨부화일 업로드를 위한 Start -->
						<c:if test="${bizModVO.fileSeq eq null || bizModVO.fileSeq eq ''}">
						<input type="hidden" name="fileListCnt" value="0" />
						<input name="atchFileAt" type="hidden" value="N">
						</c:if>
						<c:if test="${bizModVO.fileSeq ne null && bizModVO.fileSeq ne ''}">
						<input name="atchFileAt" type="hidden" value="Y">
						</c:if>
						
					    <div id="file_upload_posbl"  style="display:none;" >
						<!-- attached file Start -->
						<div>
							<div class="egov_file_box">
							<label for="egovfile_1" id="file_label" style="background-color:#008f91;coloe:#ffffff;padding:.45em .75em;" >파일선택</label> 
							<input type="file" name="file_1" id="egovfile_1"> 
							</div>
							<div id="egovComFileList"></div>
						</div>
						<!-- attached file End -->
						</div>
						<div id="file_upload_imposbl"  style="display:none;" >
				            <table width="100%" cellspacing="0" cellpadding="0" border="0" align="left" class="UseTable">
							    <tr>
							        <td style="padding:0px 0px 0px 0px;margin:0px 0px 0px 0px;">파일선택</td>
							    </tr>
				   	        </table>
						</div>
                   
                   
<!--                    
	                   <input type="text" class="form-control  " style="width:70%" value=""><a href="#" class="btn btn-table ">파일불러오기</a>
	                   <p>사용자 메뉴얼 v3.1 <span >l</span> 2.3MB<span >l</span>  <a href="#"><img src="images/icon_file_delete.png" ></a></p>
	                    <p>관망관리시스템 사용자 메뉴얼 v3.1 <span >l</span> 2.3MB<span >l</span>  <a href="#"><img src="images/icon_file_delete.png" >
 -->	                                       
                   </td>
                 </tr>                
              </tbody>
            </table>
          </div>
          <div class="btn_group right "  >
           
            <button id="btnAprv" class="btn btn-default" onclick="JavaScript:fnAprv(); return false;"> <i class="fa fa-circle-o" aria-hidden="true"></i> 승인</button>
            <button id="btnSave" class="btn btn-default" onClick="javascript:fnFileUpdate('<c:out value="${bizDtl.bizSeq}"/>'); return false;"> <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
            <button id="btnCancel" class="btn btn-default " onclick="JavaScript:fnListPage(); return false;"> <i class="fa fa-reply" aria-hidden="true"   ></i> 취소</button>
             
          </div>
          
          <!-- //write --> 
          
          </form:form>
          
        </div>
        


</body>
</html>

