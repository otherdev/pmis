<%--
  Class Name : chgUpdt.jsp
  Description : 공정변경상세조회, 수정 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>공정변경 상세 및 수정</title>

<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	
	gfn_init();
	
	/// 1.콤보초기화 및 세팅
	//사업구분 콤보 공통코드
	gfn_setCombo("bizCat", "BIZN", "${bizDtl.bizCat}");
	
	//사업명 콤보
	gfn_setCbOption($("#bizSeq"), {	
		sqlId : "selectBizMngList2",
		sCd : "bizSeq", 
		sNm : "bizNm",
		bizCat : "${bizDtl.bizCat}",
		defaultVal : "${bizDtl.bizSeq}",
	});

	//공정명 콤보
	gfn_setCbOption($("#schedSeq"), {	
		sqlId : "selectSchedList2",
		sCd : "schedSeq", 
		sNm : "schedNm",
		bizSeq : "${bizDtl.bizSeq}",
		defaultVal : "${bizDtl.schedSeq}",
	});
	
	
	/// 2.댓글목록 html append
	gfn_selectList({sqlId: 'selectReplList', modiSeq:'${bizDtl.modiSeq}'}, function(ret){
		var lst = [];
		lst = ret.data;
		
		// 배열데이터순회
		$("#replList > ul").remove();
		$.each(lst, function(idx, map){
			//var html_row = "<tr>	<td colspan='2' class='cmtname'  >" +map['regInfo']+ "</span> </p></td><td style='width:120px;'><a class='btnRepl' abbr='"+map['regId']+"' href='#' onclick='fnSaveRelp("+map['modiSeq']+","+map['replSeq']+","+idx+");'><img src='/pmis/images/icon_co_edit.png'  alt='수정 아이콘'> 수정</a>&nbsp;&nbsp;<a href='#' class='btnRepl' abbr='"+map['regId']+"' onclick='fnDelRelp("+map['modiSeq']+","+map['replSeq']+");'><img src='/pmis/images/icon_co_delete.png'  alt='삭제 아이콘'> 삭제</a> </td></tr>			<tr>	<td>상태변경</td>	<td >		<select  class='form-control input-inline' style='width:25%'  name='stat_"+idx+"' id='stat_"+idx+"' >		</select>	</td>	<td ></td></tr><tr>	<td colspan='2'> <textarea class='form-control' id='contents_"+idx+"' name='contents' rows='60' style=' width:100%; height:60px'>" +map['contents']+ "</textarea></td>	<td class='right' ></td></tr>";
			
			var html_row = "<ul class='comment-group'><li><div class='comment' ><p >" +map['regInfo']+ "<span class=' f_r'> <a href='#' class='btnRepl' abbr='"+map['regId']+"' href='#' onclick='fnSaveRelp("+map['modiSeq']+","+map['replSeq']+","+idx+");'><img src='/pmis/images/icon_co_edit.png'  alt='수정 아이콘'> 수정</a> <a href='#' class='btnRepl' abbr='"+map['regId']+"' onclick='fnDelRelp("+map['modiSeq']+","+map['replSeq']+");'><img src='/pmis/images/icon_co_delete.png'  alt='삭제 아이콘'> 삭제</a> </span> </p><p class='cmttxt'>상태변경: <select  class='form-control input-inline' style='width:25%'  name='stat_"+idx+"' id='stat_"+idx+"' >		</select></p><p class='cmttxt'><textarea class='form-control' id='contents_"+idx+"' name='contents' rows='60' style=' width:100%; height:60px'>" +map['contents']+ "</textarea></p></div></li></ul>";
			
			$("#replList").append(html_row)
		});
		
		
		// 각행의 상태콤보 초기화 및 세팅
		$.each(lst, function(idx, map){
			gfn_setCombo("stat_"+idx, "MODI", map['stat']);
		});
		
	});	

	//사업구분 콤보 공통코드
	gfn_setCombo("stat", "MODI");

	
	
	
	
	// 사업구분 변경이벤트
	$("#bizCat").on("change", function(d){
		
		//사업명 재조회
		gfn_setCbOption($("#bizSeq"), {	
			sqlId : "selectBizMngList2",
			sCd : "bizSeq", 
			sNm : "bizNm",
			bizCat : $(this).val(),
			//defaultVal : "",
		});
		
	});
	
	// 사업마스터 변경이벤트
	$("#bizSeq").on("change", function(d){
		
		//공정명 재조회
		gfn_setCbOption($("#schedSeq"), {	
			sqlId : "selectSchedList2",
			sCd : "schedSeq", 
			sNm : "schedNm",
			bizSeq : $(this).val(),
			//defaultVal : "",
		});
	});
	

	
	
	// 버튼권한처리
	setBtnAuth();	
});










/// 로컬함수 ///////////////////

function fnListPage(){
	location.href = '/pmis/pms/biz/bizChgList.do';
    //document.bizModVO.action = "<c:url value='/pms/biz/bizChgList.do'/>";
    //document.bizModVO.submit();
}
 
function fnRefresh(){
	location.reload();
    //document.bizModVO.action = "<c:url value='/pms/biz/chgUpdt.do'/>";
    //document.bizModVO.submit();
}

// 공정변경 마스터 저장
function fnUpdate(){
    //if(validateUserManageVO(document.bizModVO)){
    //}
    //document.bizModVO.submit();
	
    
    if(!gfn_formValid("frm"))	return;
    
	if(!confirm("저장하시겠습니까?")) return;
	
	// 셀렉트박스 폼데이터 전송을위해서 임시로 disabled 해제
	$("select").attr("disabled",false);
	
	gfn_saveForm("frm", "/pmis/pms/saveChgMst.do" , function(data){
		
		if(data.result){
			alert("데이터가 저장되었습니다.");
			if("${mode}" == "ADD"){
				fnListPage();
			}
			else{
				fnRefresh();
			}
		}else{
			alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
		}
	});
}

// 공정변경 마스터 삭제
function fnDelete(){
	
	if(!confirm("삭제 하시겠습니까?")) return;
	
	document.frm.delYn.value = "Y";
	gfn_saveForm("frm", "/pmis/pms/saveChgMst.do" , function(data){
		
		if(data.result){
			alert("정상적으로 삭제되었습니다.");
		}else{
			alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
		}
	});
}


// 리플등록
function fnReply(){
	
    if(!confirm("공정변경 의견을 등록하시겠습니까?"))	return;
    
	gfn_saveCmm({sqlId:"saveChgRepl", data:{modiSeq: "${bizDtl.modiSeq}", replSeq: "", stat:$("#stat").val(), contents:$("#contents").val(), mode: "ADD"}  }
	,function(data){
		if (data.result) {
			alert("등록 되었습니다.");
			fnRefresh();
			return;
		}		
		else{
			alert("저장에 실패하였습니다.","E");
			return;
		}
	});
    
}

// 리플수정
var fnSaveRelp = function(modiSeq, replSeq, idx){
    if(!confirm("공정변경 의견을 수정하시겠습니까?"))	return;
    
	gfn_saveCmm({sqlId:"saveChgRepl", data:{modiSeq: modiSeq, replSeq: replSeq, mode: "MOD", stat:$("#stat_"+idx).val(), contents:$("#contents_"+idx).val()}  }
		,function(data){
			if (data.result) {
				alert("수정 되었습니다.");
				return;
			}		
			else{
				alert("저장에 실패하였습니다.","E");
				return;
			}
	});
}

// 리플삭제
var fnDelRelp = function(modiSeq, replSeq){
	
    if(!confirm("공정변경 의견을 삭제하시겠습니까?"))	return;
    
	gfn_saveCmm({sqlId:"saveChgRepl", data:{modiSeq: modiSeq, replSeq: replSeq, mode: "MOD", delYn: "Y", stat:"MODI01"}  }
	,function(data){
		if (data.result) {
			alert("삭제 되었습니다.");
			fnRefresh();
			return;
		}		
		else{
			alert("저장에 실패하였습니다.","E");
			return;
		}
	});
}


	/// 버튼및권한처리
	var setBtnAuth = function(){
		//alert("${bizDtl.regId}");
		switch("${mode}"){
		case "ADD" :
			$("#btnSave").attr("disabled",false);
			$("#btnDel").attr("disabled",true);
			
			//신규댓글
			$("#divReplTable").hide();
			
			//개인댓글
			$(".btnRepl").hide();
			break;
			
		case "MOD" :		
			debugger;
			// 저장버튼 - 요청상태일때, 관리자나 자신만 수정가능 
			if( "${bizDtl.stat}" == "MODI01" && 
					("${isBizMngYn}" == "Y" || "${bizDtl.regId}" == "${sessionScope.id}")){
				$("#btnSave").attr("disabled",false);
				$("#btnDel").attr("disabled",false);
			}
			else{
				$("#btnSave").attr("disabled",true);
				$("#btnDel").attr("disabled",true);
			}
				
			
			//신규댓글
			if("${isBizMngYn}" == "Y")
				$("#divReplTable").show(); //사업관리자만 보임			
			else
				$("#divReplTable").hide();			
			
			//개인댓글 - 요청상태에서 자기글만 수정가능..
			$(".btnRepl").each(function(){
				if($(this).attr("abbr") == "${sessionScope.id}" && "${bizDtl.stat}" == "MODI01" )
					$(this).show();						
				else
					$(this).hide();						
			});
			break;
			
			//$("#USER_ID").removeAttr("readonly");				
			//$("#btnChk").attr("disabled",false);
			break;
		};
		
		
		// 수정모드에서는 콤보 비활성화
		if('${mode}' == 'MOD'){
			$("select").attr("disabled",true);
		}
		$("select[name^='stat']").attr("disabled",false);
		
	}

//-->
</script>

</head>
<body>
<noscript>자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>    

        
        <div class="conts"  >
          <h1><i class="fa fa-chevron-circle-right  "></i> 공정변경관리</h1>
          
           <!-- write -->
		   <form id="frm" name="frm">
		   
		   <input type="hidden" name="modiSeq" value="${bizDtl.modiSeq}">
		   <input type="hidden" name="mode" value="${mode}">
		   <input type="hidden" name="stat" value="${bizDtl.stat}">
		   <input type="hidden" name="delYn" value="N">
          
          <div class="table-responsive">
            <table class="table table-view">
              <caption>공정변경관리</caption>
              <colgroup>
              <col width="15%">
              <col width="85%">
              </colgroup>
              <tbody>
                <tr>
                  <th>사업구분</th>
                  <td>
                    <select class="form-control input-inline" style="width:400px;"  id="bizCat"  >	                    
                    </select>
                  	
                  </td>
                </tr>
                <tr>
                  <th>사업명</th>
                  <td>
                    <select class="form-control input-inline" style="width:400px;" name="bizSeq"  id="bizSeq"  >	                    
                    </select>
                  
                  </td>
                </tr>
                 <tr>
                   <th>공정명</th>
                   <td>
                    <select class="form-control input-inline" style="width:400px;" name="schedSeq"   id="schedSeq" >	                    
                    </select>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <c:if test="${mode=='MOD'}">진행상태 : <strong>${bizDtl.statNm}</strong></c:if>
                   	</td>
                 </tr>
                 <tr>
                   <th class="req">제목</th>
                   <td>
					<input id="ttl" name="ttl" class="form-control reqVal"  size="100"  maxlength="60" value="${bizDtl.ttl}"/>
                   </td>
                 </tr>
                 <tr>
                   <th>내용</th>
                   <td >
                    <textarea id="contents" name="contents" class="form-control" style="height:200px;" rows="60" >${bizDtl.contents}</textarea>
                   </td>
                 </tr>
              </tbody>
            </table>
          </div>
          </form>
          
          
          <div class="btn_group right "  >
          	<button id="btnSave" class="btn btn-default" onclick="JavaScript:fnUpdate(); return false;"> <i class="fa fa-edit" aria-hidden="true"></i> 저장</button>
            <button id="btnDel" class="btn btn-default " onclick="JavaScript:fnDelete(); return false;"> <i class="fa fa-close" aria-hidden="true"></i> 삭제</button>
            <button id="btnCancel" class="btn btn-default " onclick="JavaScript:fnListPage(); return false;"> <i class="fa fa-reply" aria-hidden="true"></i> 취소</button>
          </div>
          
          <!-- //write --> 
          
          
           	<!-- 댓글목록 -->
          	<div class="comment-group" id="replList">
			</div>            
          
          
          <!--comment-write-->
          <div class="comment-write" id="divReplTable">
           
            <table class="table table-border-no table-comment" >
            
             <colgroup>
	            <col width="80px">
	            <col width="*">
	            <col width="90px">
            </colgroup>
            
            <!-- 댓글신규 -->
           	<tbody id="divRepl">
            	
	              <tr>
	                <td colspan="3" class="cmtname"  >${sessionScope.name} [${sessionScope.id}]</td>
	              </tr>
	              <tr>
	                <td>상태변경</td>
	                <td >
		                <select  class="form-control input-inline" style="width:25%"  name="stat" id="stat" >
		                </select>
	                </td>
	                <td ></td>
	              </tr>
	              <tr>
	                <td colspan="2"> <textarea class="form-control" id="contents" name="contents" rows="60" style=" width:100%; height:60px"></textarea></td>
	                <td class="right" ><a href="#" class="btn btn-table " style="height:50px; width:70px; vertical-align:middle; line-height:50px;" onclick="fnReply();"> <i class="fa fa-plus "></i> 등록</a></td>
	              </tr>
              
              </tbody>
            </table>
          </div>
          
          <!--//comment-write-->           
          
        </div>
        


</body>
</html>

