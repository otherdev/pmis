<%--
  Class Name : bizUpdt.jsp
  Description : 작업상세조회, 수정 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>사업관리 상세 및 수정</title>

<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>

<validator:javascript formName="bizModVO" staticJavascript="false" xhtml="true" cdata="false"/>
<script type="text/javascript" src="<c:url value='/js/pms/biz/bizUpdt.js' />" ></script>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	gfn_init();
	
	if('${mode}' == 'MOD'){
		//$("select").attr("disabled",true);
	}
	
	$( "#bizStartDt,#bizEndDt" ).datepicker({
      showOn: "button",
      buttonImage: "/pmis/images/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Select date",
      dateFormat: "yy-mm-dd"
    });

	// 1.공정그리드
	// 공통코드 가져오기
	gfn_selectList({sqlId: 'selectCmmCd', mstCd:'PROC'}, function(ret){
		PROC_LIST = ret.data;
		
		// 공정리스트
		gfn_selectList({sqlId: 'selectSchList', bizSeq:'${bizSeq}'}, function(ret){
	
			lst = ret.data;
			
			// 그리드 초기화
			fnObj.pageStart();	
		});	
	});	

	
	// 2.팀원그리드
	// 사용자콤보 가져오기
	gfn_selectList({sqlId: 'selectUserList', ofcCd:''}, function(ret){
		USER_LIST = ret.data;
		
		// 공통코드 가져오기
		gfn_selectList({sqlId: 'selectCmmCd', mstCd:'POSI'}, function(ret){
			POSI_LIST = ret.data;
			
			// 공정리스트
			gfn_selectList({sqlId: 'selectMpwList', bizSeq:'${bizSeq}'}, function(ret){
				lst2 = ret.data;
				
				// 그리드 초기화
				fnObj2.pageStart();	
			});	
		});	
	});		
	
	
	
	// 메시지 처리 
	var sResultMsg = '<c:if test="${!empty resultMsg}"><spring:message code="${resultMsg}" /></c:if>';
	if(sResultMsg.length != 0 ) {
		alert(sResultMsg);
	}
	
});

function fnListPage(){
    document.bizModVO.action = "<c:url value='/pms/biz/bizMngList.do'/>";
    document.bizModVO.submit();
}
function fnDelete() {
    if(confirm("<spring:message code="common.delete.msg" />")){
        document.bizModVO.action = "<c:url value='/pms/biz/bizDelete.do'/>";
        document.bizModVO.submit(); 
    }
}
function fnRefresh(){
    document.bizModVO.action = "<c:url value='/pms/biz/bizUpdt.do'/>";
    document.bizModVO.submit();
}


// 저장
function fnUpdate(){
	myGrid.editCellClear(0,0,0);
	myGrid.selectClear();
	myGrid2.editCellClear(0,0,0);
	myGrid2.selectClear();

	// ajax 저장 - 공정, 팀원
	var _lst = fnObj.grid.getList();
	var _lst2 = fnObj2.grid.getList();
	var ll = fnObj.grid.getSelectedItem();	
	var ll2 = myGrid.getSelectedItem();	

	
    if(!gfn_formValid())	return;

    //합계비율체크
    var sum = 0;
    $.each(lst, function (idx, map) {
    	try{
	        sum += map.planRate;
    	}catch(e){}
    });
    if(sum != 100){
    	alert("비율은 100%가 되어야합니다.");
    	return;
    }
    
	if(!confirm("저장하시겠습니까?")) return;
	
 	
	//저장처리
	gfn_saveList(
			"/pmis/pms/saveBizDtls.do"
			,{lst: _lst, sqlId: 'updateSchedList', lst2: _lst2, sqlId2: 'updateMpwList', bizSeq: '${bizSeq}'}
			, function(data){
				if (data.result) {
					
					// 저장후 사업마스터 키 세팅(신규등록 처리를 위해..)
			        document.bizModVO.bizSeq.value = data.bizSeq;
					
					//사업마스터 저장
					//if(validateUserManageVO(document.bizModVO)){
				    //}
			        document.bizModVO.action = "<c:url value='/pms/biz/updateBiz.do'/>";  
			        document.bizModVO.submit();
					
					//alert("성공적으로 저장되었습니다.");
					return;
				}
				else{
					alert("저장에 실패하였습니다.","E");
					return;
				}
	});
 
 
}
//-->

'<c:if test="${!empty resultMsg}">alert("<spring:message code='${resultMsg}' />");</c:if>'
</script>




</head>
<body>
<noscript>자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>    

<!-- container -->
<div id="container"  class="content" > 
  
  <!-- content -->
  <div id="content" > 
    
    <!--cont-box-->
    
    <div class="contents-box"  > 
      
      <!--conts-->
      <div class="conts"  >
        <h1><i class="fa fa-chevron-circle-right  "></i> 사업관리 </h1>
        
       	<!-- write -->
       	<form:form commandName="bizModVO"  onkeydown="return gfn_fireKey(event);" name="bizModVO" method="post" >
	        <!-- 상세정보 작업 삭제시 prameter 전달용 input -->
	        <input name="checkedIdForDel" type="hidden" />
            <form:hidden path="bizSeq" /> 
            <form:hidden path="schedSeq" />
			<form:hidden path="workSeq" /> 
            <form:hidden path="mode" />
	        
	        
			<!-- 	        검색조건 유지 -->
	        <input type="hidden" name="searchCondition" value="<c:out value='${bizModVO.searchCondition}'/>"/>
	        <input type="hidden" name="searchKeyword" value="<c:out value='${bizModVO.searchKeyword}'/>"/>
	        <input type="hidden" name="pageIndex" value="<c:out value='${bizModVO.pageIndex}'/>"/>
	        
	        <div class="table-responsive">
	          <table class="table table-write">
	            <caption>
	            	사업관리
	            </caption>
	            <colgroup>
	            <col width="20%">
	            <col width="80%">
	            </colgroup>
	            <tbody>
	              <tr>
	                <th class="req">사업구분</th>
	                <td>
	                    <form:select class="form-control input-inline reqVal" style="width:25%"  path="bizCat" id="bizCat"      >
							<c:if test="${mode == 'ADD'}">
		                        <form:option value="" label="--선택하세요--"/>
	                    	</c:if>
	                        <form:options items="${BIZN_result}" itemValue="dtlCd" itemLabel="cdNm"/>
	                    </form:select>
	                    <form:errors path="bizCat" cssClass="error"/>
	                  </td>
	              </tr>
	               <tr>
	                 <th class="req">사업명</th>
	                 <td>
	                    <form:input path="bizNm" id="bizNm" class="form-control reqVal"  size="20"  maxlength="60" />
	                    <form:errors path="bizNm" cssClass="error" />
               		 </td>
	               </tr>
	              <tr>
	                <th>사업기간</th>
	                <td>
	                    <form:input path="bizStartDt" id="bizStartDt" class="form-control" style="width:150px;" size="20"  maxlength="60" />
	                    <form:errors path="bizStartDt" cssClass="error" /> ~
	                    <form:input path="bizEndDt" id="bizEndDt" class="form-control"  style="width:150px;" size="20"  maxlength="60" />
	                    <form:errors path="bizEndDt" cssClass="error" />
                  	</td>
	              </tr>
	              <tr>
	                <th>개요</th>
	                <td>
	                    <form:textarea path="bizDesc" id="bizDesc" class="form-control" style="height:200px;" rows="60" />
	                    <form:errors path="bizDesc" cssClass="error" />
	                </td>
	              </tr>
	              <tr>
	                <th class="num">사업비용</th>
	                <td>
	                    <form:input path="bizCost" id="bizCost" class="form-control numVal" style="width:150px;" rows="60" />
	                    <form:errors path="bizCost" cssClass="error" /> 원
                  	</td>
	              </tr>
					<tr>
					  <th>공정계획</th>
					  <td>
		                <div style="padding:10px;">
		                    <input type="button" value="추가" class="AXButton Blue" onclick="fnObj.grid.append();"/>
		                    <input type="button" value="삭제" class="AXButton Blue" onclick="fnObj.grid.remove();"/>
		                </div>					  	
					  	<div id="grdSched" style="height:300px;"></div>
					  </td>
					</tr>
					<tr>
					  <th>팀원구성</th>
					  <td>
		                <div style="padding:10px;">
		                    <input type="button" value="추가" class="AXButton Blue" onclick="fnObj2.grid.append();"/>
		                    <input type="button" value="삭제" class="AXButton Blue" onclick="fnObj2.grid.remove();"/>
		                </div>					  	
					  	<div id="grdTeam" style="height:300px;"></div>
					  </td>
					</tr>
	             
	            </tbody>
	          </table>
	        </div>
	        
	        
	        <div class="btn_group right "  >
	            <button id="button" class="btn btn-default " onclick="fnDelete(); return false;" ><i class="fa fa-close" aria-hidden="true"></i> 삭제</button>
	          	<button id="button" class="btn btn-default" onclick="JavaScript:fnUpdate(); return false;" > <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
	           	<button id="button" class="btn btn-default" onclick="JavaScript:fnListPage(); return false;" > <i class="fa fa-reply" aria-hidden="true"></i> 취소</button>
	        </div>
        

        </form:form>
        <!-- //write --> 
        
      </div>
      
      <!--conts--> 
      
    </div>
    
    <!--//cont-box--> 
    
  </div>
  <!-- //content --> 
</div>
<!-- //container --> 

</body>
</html>

