<%--
  Class Name : workUpdt.jsp
  Description : 작업상세조회, 수정 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>작업 상세 및 수정</title>

<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	gfn_init();

	if('${mode}' == 'MOD'){
		$("select").attr("disabled",true);
	}
	
	$( "#workStartDt,#workEndDt" ).datepicker({
      showOn: "button",
      buttonImage: "/pmis/images/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Select date",
      dateFormat: "yy-mm-dd"
    });	
	
	// 버튼권한처리
	setBtnAuth();	
	
});

function fnListPage(){
    document.bizModVO.action = "<c:url value='/pms/biz/workMngList.do'/>";
    document.bizModVO.submit();
}
function fnDelete(checkedIds) {
    if(confirm("<spring:message code="common.delete.msg" />")){
        document.bizModVO.action = "<c:url value='/pms/biz/deleteWork.do'/>";
        document.bizModVO.submit(); 
    }
}
function fnRefresh(){
    document.bizModVO.action = "<c:url value='/pms/biz/workUpdt.do'/>";
    //$("input[name='bizSeq']").val($("#bizSeq").val());
    //$("input[name='schedSeq']").val($("#schedSeq").val());
    document.bizModVO.bizSeq.value = $("#bizSeq").val();
    document.bizModVO.schedSeq.value = $("#schedSeq").val();
    document.bizModVO.workSeq.value = $("input[name='workSeq']").val();
    document.bizModVO.mode.value = "${mode}";
    document.bizModVO.submit();
}
function fnUpdate(){
    //if(validateUserManageVO(document.bizModVO)){
    //}
    
    if(!gfn_formValid("frm"))	return;

	if(!confirm("저장하시겠습니까?")) return;
    
    document.bizModVO.bizSeq.value=$("#bizSeq").val();
    document.bizModVO.schedSeq.value=$("#schedSeq").val();
    document.bizModVO.workSeq.value=$("input[name='workSeq']").val();
    $("input[name='bizSeq']").val($("#bizSeq").val());
    $("input[name='schedSeq']").val($("#schedSeq").val());
 	document.bizModVO.submit();
}


//확인처리
function fnAprv(){

    if(!confirm("작업을 승인하시겠습니까?"))	return;
    
	gfn_saveCmm({sqlId:"updateWorkAprv", data:{bizSeq: "${bizModVO.bizSeq}", schedSeq: "${bizModVO.schedSeq}", workSeq: "${bizModVO.workSeq}", }  }
	,function(data){
		if (data.result) {
			alert("승인 되었습니다.");
			fnRefresh();
			return;
		}		
		else{
			alert("저장에 실패하였습니다.","E");
			return;
		}
	});
}


/// 버튼및권한처리
var setBtnAuth = function(){

	switch("${bizModVO.aprvYn}"){
		
		case "Y" : //승인
			// 승인버튼 
			$("#btnAprv").attr("disabled",true);
			$("#btnSave").attr("disabled",true);
			$("#btnDel").attr("disabled",true);
			
			break;
			
		case "N" : //미승인		
			// 승인버튼 - 관리자만 수정가능 
			if("${isBizMngYn}" == "Y"  || "${isAdmMngYn}" == "Y" ){
				$("#btnAprv").attr("disabled",false);
			}
			else{
				$("#btnAprv").attr("disabled",true);
			}
			
			//본인,관리자만 수정가능
			if("${isBizMngYn}" == "Y" || "${sessionScope.id}" == "${bizModVO.regId}" || "${isAdmMngYn}" == "Y" ){
				$("#btnSave").attr("disabled",false);
				$("#btnDel").attr("disabled",false);
			}
			else{
				$("#btnSave").attr("disabled",true);
				$("#btnDel").attr("disabled",true);
			}
		
			break;
			
		default :	
			break;
	};
	
	
}


//-->
</script>

</head>
<body>
<noscript>자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>    

<!-- container -->
<div id="container"  class="content" > 
  
  <!-- content -->
  <div id="content" > 
    
    <!--contents-box-->
    
    <div class="contents-box"  > 
      
      <!--conts-->
      <div class="conts"  >
        <h1><i class="fa fa-chevron-circle-right  "></i> 작업관리 </h1>
        
       	<!-- write -->
       	<form:form commandName="bizModVO" action="${pageContext.request.contextPath}/pms/biz/updateWork.do" name="bizModVO" method="post" >
	        <!-- 상세정보 작업 삭제시 prameter 전달용 input -->
	        <input name="checkedIdForDel" type="hidden" />
	        
			<!-- 	        검색조건 유지 -->
	        <input type="hidden" name="searchCondition" value="<c:out value='${bizModVO.searchCondition}'/>"/>
	        <input type="hidden" name="searchKeyword" value="<c:out value='${bizModVO.searchKeyword}'/>"/>
	        <input type="hidden" name="pageIndex" value="<c:out value='${bizModVO.pageIndex}'/>"/>
	        
			<input type="hidden" name="bizSeq" value="<c:out value='${bizModVO.bizSeq}'/>"/> 
            <input type="hidden" name="schedSeq" value="<c:out value='${bizModVO.schedSeq}'/>"/>
			<input type="hidden" name="workSeq" value="<c:out value='${bizModVO.workSeq}'/>"/> 
            <input type="hidden" name="mode" value="<c:out value='${bizModVO.mode}'/>"/>
	        
	        <div class="table-responsive">
	          <table class="table table-write">
	            <caption>
	            	작업관리
	            </caption>
	            <colgroup>
	            <col width="20%">
	            <col width="80%">
	            </colgroup>
	            <tbody>
	              <tr>
	                <th>사업구분</th>
	                <td>
	                    <form:select class="form-control input-inline" style="width:25%"  path="bizCat" id="bizCat" onchange="javascript:fnRefresh();"     >
							<c:if test="${mode == 'ADD'}">
		                        <form:option value="" label="--선택하세요--"/>
	                    	</c:if>
	                        <form:options items="${BIZN_result}" itemValue="dtlCd" itemLabel="cdNm"/>
	                    </form:select>
	                    <form:errors path="bizCat" cssClass="error"/>
	                  </td>
	              </tr>
	              <tr>
	                <th>사업명</th>
	                <td>
	                    <form:select class="form-control input-inline" style="width:25%"  path="bizSeq" id="bizSeq"  onchange="javascript:fnRefresh();"   >
							<c:if test="${mode == 'ADD'}">
		                        <form:option value="-1" label="--선택하세요--"/>
	                    	</c:if>
	                        <form:options items="${BIZS_result}" itemValue="BIZ_SEQ" itemLabel="BIZ_NM"/>
	                    </form:select>
	                    <form:errors path="bizSeq" cssClass="error"/>
	                </td>
	              </tr>
	               <tr>
	                 <th>공정명</th>
	                 <td>
	                    <form:select class="form-control input-inline" style="width:25%"  path="schedSeq" id="schedSeq" > 
							<c:if test="${mode == 'ADD'}">
		                        <form:option value="-1" label="--선택하세요--"/>
	                    	</c:if>
	                        <form:options items="${SCHED_result}" itemValue="SCHED_SEQ" itemLabel="SCHED_NM"/>
	                    </form:select>
	                    <form:errors path="schedSeq" cssClass="error"/>
	                 </td>
	               </tr>
						               <tr>
						                 <th>공정구분</th>
						                 <td>
						                    <form:select class="form-control input-inline" style="width:25%"  path="schedCat" id="schedCat">
												<c:if test="${mode == 'ADD'}">
							                        <form:option value="" label="--선택하세요--"/>
						                    	</c:if>
						                        <form:options items="${PROC_result}" itemValue="dtlCd" itemLabel="cdNm"/>
						                    </form:select>
						                    <form:errors path="schedCat" cssClass="error"/>
						                 </td>
						               </tr>
	               <tr>
	                 <th class="req">제목</th>
	                 <td>
	                    <form:input path="ttl" id="ttl" class="form-control reqVal"  size="20"  maxlength="60" />
	                    <form:errors path="ttl" cssClass="error" />
               		 </td>
	               </tr>
	              <tr>
	                <th>내용</th>
	                <td>
	                    <form:textarea path="contents" id="contents" class="form-control" style="height:200px;" rows="60" />
	                    <form:errors path="contents" cssClass="error" />
	                </td>
	              </tr>
	             
	              <tr>
	                <th>투입기간</th>
	                <td>
	                    <form:input path="workStartDt" id="workStartDt" class="form-control" style="width:150px;" size="20"  maxlength="60" />
	                    <form:errors path="workStartDt" cssClass="error" /> ~
	                    <form:input path="workEndDt" id="workEndDt" class="form-control"  style="width:150px;" size="20"  maxlength="60" />
	                    <form:errors path="workEndDt" cssClass="error" />
                  	</td>
	              </tr>
	              <tr>
	                <th class="num">투입공수</th>
	                <td>
	                    <form:input path="runMpw" id="runMpw" class="form-control numVal" style="width:150px;" rows="60" />
	                    <form:errors path="runMpw" cssClass="error" />M/M
                  	</td>
	              </tr>
	            </tbody>
	          </table>
	        </div>
	        
	        
	        <div class="btn_group right "  >
            	<button id="btnAprv" class="btn btn-default" onclick="JavaScript:fnAprv(); return false;"> <i class="fa fa-circle-o" aria-hidden="true"></i> 확인</button>
	            <button id="btnDel" class="btn btn-default " onclick="fnDelete(); return false;" ><i class="fa fa-close" aria-hidden="true"></i> 삭제</button>
	          	<button id="btnSave" class="btn btn-default" onclick="JavaScript:fnUpdate(); return false;" > <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
	           	<button id="button" class="btn btn-default" onclick="JavaScript:fnListPage(); return false;" > <i class="fa fa-reply" aria-hidden="true"></i> 취소</button>
	        </div>
        

        </form:form>
        <!-- //write --> 
        
      </div>
      
      <!--conts--> 
      
    </div>
    
    <!--//contents-box--> 
    
  </div>
  <!-- //content --> 
</div>
<!-- //container --> 

</body>
</html>

