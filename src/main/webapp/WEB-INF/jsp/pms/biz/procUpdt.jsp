<%--
  Class Name : procUpdt.jsp
  Description : 작업상세조회, 수정 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>작업 상세 및 수정</title>

<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	
	gfn_init();
	
	if('${mode}' == 'MOD'){
		$("select").attr("disabled",true);
	}

	// 버튼권한처리
	setBtnAuth();	
	
});

function fnListPage(){
	location.href = '/pmis/pms/biz/bizProcList.do';
    //document.bizModVO.action = "<c:url value='/pms/biz/bizProcList.do'/>";
    //document.bizModVO.submit();
}
 
function fnRefresh(){
	location.reload();
    //document.bizModVO.action = "<c:url value='/pms/biz/procUpdt.do'/>";
    //document.bizModVO.submit();
}

function fnUpdate(){
    //if(validateUserManageVO(document.bizModVO)){
    //}
    //document.bizModVO.submit();
    
    // 저장배열만들기
    var ary = [];
    $("input[name='runRate']").each(function(d){
    	console.log("runRate......");
	    var data = {};
    	data.bizSeq = '${bizDtl.bizSeq}';
    	data.schedSeq = $(this).attr("abbr");
    	data.runRate = $(this).val();
    	
    	ary.push(data);
    });
    //alert("ary - " + JSON.stringify(ary));
    
    if(!confirm("실적진행율 저장하시겠습니까?"))	return;
    
    gfn_saveCmmList({sqlId:"updateRunRate", lst:ary  }
	,function(data){
		if (data.result) {
			alert("성공적으로 저장되었습니다.");
			fnRefresh();
			return;
		}		
		else{
			alert("저장에 실패하였습니다.","E");
			return;
		}
	});
}

/// 버튼및권한처리
var setBtnAuth = function(){

	
	// 승인버튼 - 관리자만 수정가능 
	if("${isBizMngYn}" == "Y"  || "${isAdmMngYn}" == "Y" ){
		$("#btnSave").attr("disabled",false);
	}
	else{
		$("#btnSave").attr("disabled",true);
	}
	
	
}
//-->
</script>

</head>
<body>
<noscript>자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>    

        
        <div class="conts"  >
          <h1><i class="fa fa-chevron-circle-right  "></i> 사업진행현황</h1>
          
           <!-- write -->
          
          <div class="table-responsive">
            <table class="table table-view">
              <caption>사업진행현황</caption>
              <colgroup>
              <col width="15%">
              <col width="85%">
              </colgroup>
              <tbody>
                <tr>
                  <th>사업구분</th>
                  <td>${bizDtl.bizCatNm}</td>
                </tr>
                <tr>
                  <th>사업명</th>
                  <td>${bizDtl.bizNm}</td>
                </tr>
                 <tr>
                   <th>사업기간</th>
                   <td>${bizDtl.bizTerm}</td>
                 </tr>
                 <tr>
                   <th>개요</th>
                   <td>${bizDtl.bizDesc}</td>
                 </tr>
                 <tr>
                   <th class="num">사업비용</th>
                   <td class="numVal left" >${bizDtl.bizCost} KRW</td>
                 </tr>
                <tr>
                  <th>공정계획</th>
                  <td>
                     <!--table list -->
                   <table class="table table-in">
                <colgroup>
                <col width="8%">
                <col width="12%">
                <col width="15%">
                <col width="12%">
                <col width="12%">
                <col width="8%">
                <col width="8%">
                <col width="8%">
                <col width="8%">
                <col width="8%">
                </colgroup>
                <thead>
                  <tr>
                    <th rowspan="2">선택</th>
                    <th rowspan="2">공정구분</th>
                    <th rowspan="2">공정명</th>
                    <th colspan="2">사업기간</th>
                    <th colspan="2">진행율(%)</th>
                    <th colspan="3">투입공수(M/M)</th>
                    </tr>
                  <tr>
                    <th>시작일자</th>
                    <th>종료일자</th>
                    <th>계획</th>
                    <th>진행</th>
                    <th>계획</th>
                    <th>실적</th>
                    <th>비율(%)</th>
                    </tr>
                </thead>
                <tbody>
				<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td class="lt_text3" colspan="9">
							<spring:message code="common.nodata.msg" />
						</td>
					</tr>
				</c:if>
				<c:forEach items="${resultList}" var="result" varStatus="status">
                  <tr <c:if test="${result.schedCatNm == '합계'}"> class="total" </c:if> >
                  	<c:choose >
                  		<c:when test="${result.schedCatNm == '합계'}">
                    		<td colspan="5" class="total">합계</td>
                  		</c:when>
                  		<c:otherwise>
		                    <td>${status.count}</td>
		                    <td >${result.schedCatNm}</td>
		                    <td>${result.schedNm}</td>
		                    <td>${result.schedStartDt}</td>
		                    <td>${result.schedEndDt}</td>
                  		</c:otherwise>
                  	</c:choose>
                  	
                    <td>${result.planRate}</td>
                    <td>
                  	<c:choose >
                  		<c:when test="${result.schedCatNm == '합계'}">
                  			${result.runRate}
                  		</c:when>
                  		<c:otherwise>
	                    	<input type="text" name="runRate" class="form-control" style="width:50px; text-align: right;" value="${result.runRate}" abbr="${result.schedSeq}">
                  		</c:otherwise>
					</c:choose>                    
                   	</td>
                    <td>${result.planMpw}</td>
                    <td>${result.runMpw}</td>
                    <td>${result.mpwRate}</td>
                  </tr>
				</c:forEach>

                </tbody>
              </table>
                  <!--//table list -->
                  
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="btn_group right "  >
           
          
            <button id="btnSave" class="btn btn-default" onclick="JavaScript:fnUpdate(); return false;"> <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
            <button id="button" class="btn btn-default " onclick="JavaScript:fnListPage(); return false;"> <i class="fa fa-arrow-left" aria-hidden="true"   ></i> 뒤로</button>
             
          </div>
          
          <!-- //write --> 
          
        </div>
        


</body>
</html>

