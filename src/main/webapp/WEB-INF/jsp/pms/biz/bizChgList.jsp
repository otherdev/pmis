<%--
  Class Name : bizChgList.jsp
  Description : 공정변경현황(조회) JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀 
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>공정변경현황</title>

<%@ include file="/WEB-INF/jsp/pms/include-pms.jspf"%>
<script type="text/javaScript" language="javascript" defer="defer">
<!--
$(document).ready(function() {
	gfn_init();
});



/// href 호출하면서, vo로도 키 전달하기 위해 폼에 세팅해줌
function fnView(bizSeq, schedSeq, modiSeq) {
	if(!gfn_isMyBiz(bizSeq,'${sessionScope.id}') && '${sessionScope.isAdmYn}' == 'N'){
		alert("사용자가 속한 사업이 아닙니다.");
		return;
	}
	
    document.listForm.bizSeq.value = bizSeq;
    document.listForm.schedSeq.value = schedSeq;
    document.listForm.modiSeq.value = modiSeq;
    document.listForm.mode.value = "MOD";
    document.listForm.action = "<c:url value='/pms/biz/chgUpdt.do'/>";
    document.listForm.submit();
}

function fnAdd(){
    document.listForm.mode.value = "ADD";
    document.listForm.action = "<c:url value='/pms/biz/chgUpdt.do'/>";
    document.listForm.submit();
}

function fnLinkPage(pageNo){
    document.listForm.pageIndex.value = pageNo;
    document.listForm.action = "<c:url value='/pms/biz/bizChgList.do'/>";
    document.listForm.submit();
}
function fnSearch(){
    document.listForm.pageIndex.value = 1;
    document.listForm.action = "<c:url value='/pms/biz/bizChgList.do'/>";
    document.listForm.submit();
}

'<c:if test="${!empty resultMsg}">alert("<spring:message code="${resultMsg}" />");</c:if>'
//-->
</script>

</head>
<body>
	<noscript class="noScriptTitle">자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>
	<form name="listForm" action="<c:url value='/pms/biz/bizChgList.do'/>" method="post">

	<!-- 현재위치 네비게이션 시작 -->
	<div class="conts" >
		<h1><i class="fa fa-chevron-circle-right  "></i> 공정변경현황</h1>
		<input type="submit" id="invisible" class="invisible"/>
		
		<!-- 검색 필드 박스 시작 -->
		<div class="search-group">
			<input name="bizSeq" type="hidden" value="<c:out value='${bizVO.bizSeq}'/>"/>
			<input name="schedSeq" type="hidden" value="<c:out value='${bizVO.schedSeq}'/>"/>
			<input name="modiSeq" type="hidden" value="<c:out value='${bizVO.modiSeq}'/>"/>
 			<input name="mode" type="hidden" value="<c:out value='${bizVO.mode}'/>"/>
			<input name="pageIndex" type="hidden" value="<c:out value='${bizVO.pageIndex}'/>"/>
			
			<select name="searchCondition" id="searchCondition" title="검색조건2-검색어구분" class="form-control input-inline" >
			 <option value="0" <c:if test="${empty bizVO.searchCondition || bizVO.searchCondition == '0'}">selected="selected"</c:if> >사업명</option>
			 <option value="1" <c:if test="${bizVO.searchCondition == '1'}">selected="selected"</c:if> >제목</option>
			 <option value="2" <c:if test="${bizVO.searchCondition == '2'}">selected="selected"</c:if> >등록자</option>
			</select>
					  
			<input type="text" class="form-control" value="" style="width:200px" name="searchKeyword" id="searchKeyword" >
		
			<button id="button" class="btn btn-default " onclick="fnSearch(); return false;" > <i class="fa fa-search " aria-hidden="true"></i> 검색</button>

		</div>
		<!-- //검색 필드 박스 끝 -->
		
		
		<!-- 리스트 -->
	    <div class="of-y-no">
	       <table class="table table-hover table-list center ">
	      
			<colgroup>
				<col width="60px">
				<col width="150px">
				<col width="170px">
				<col width="150px">
				<col >
				<col width="130px">
				<col width="130px">
				<col width="130px">
			</colgroup>
	        
	        
	        <thead>
	          <tr>
	            <th>번호</th>
	            <th>사업구분</th>
	            <th>사업명</th>
	            <th>공정명</th>
	            <th>제목</th>
	            <th>진행상태</th>
	            <th>등록자</th>
	            <th>등록일자</th>
	          </tr>
	        </thead>
	        <tbody>
	
				<%-- 데이터를 없을때 화면에 메세지를 출력해준다 --%>
				<c:if test="${fn:length(resultList) == 0}">
					<tr>
						<td class="lt_text3" colspan="8">
							<spring:message code="common.nodata.msg" />
						</td>
					</tr>
				</c:if>
				<c:forEach items="${resultList}" var="result" varStatus="status">
					<tr style="cursor:pointer;" onclick="javascript:fnView('${result.bizSeq}','${result.schedSeq}','${result.modiSeq}'); return false;">
						<td ><c:out value="${paginationInfo.totalRecordCount - result.rn + 1}"/></td>
						<td ><c:out value="${result.bizCatNm}"/></td>
						<td ><c:out value="${result.bizNm}"/></td>
						<td ><c:out value="${result.schedNm}"/></td>
						<td ><c:out value="${result.ttl}"/> </td>
						<td ><c:out value="${result.statNm}"/></td>
						<td ><c:out value="${result.regNm}"/></td>
						<td ><c:out value="${result.regDt}"/></td>
					</tr>
				</c:forEach>
	        </tbody>
	      </table>
	      
      </div>
	      
      <div style="width:100%; text-align:center">
        <ul class="pagination pagination-sm  " >
			<ui:pagination paginationInfo = "${paginationInfo}"  type="image" jsFunction="fnLinkPage" />
        </ul>
      </div>
	    <div class="btn_group right"  >
	      <button id="button" class="btn btn-default " onclick="fnAdd();"> <i class="fa fa-plus "></i> 등록</button>
	    </div>
   	</div>

	<!-- //content 끝 -->
    
	</form>
	        
</body>
</html>