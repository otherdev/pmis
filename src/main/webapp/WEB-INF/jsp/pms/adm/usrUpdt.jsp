<%--
  Class Name : usrUpdt.jsp
  Description : 사용자조회, 수정 JSP
  Modification Information
 
      수정일         수정자                   수정내용
    -------    --------    ---------------------------
     2017.03.03                 최초 생성
 
    author   : 공통서비스 개발팀
    since    : 2017.03.03
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>사용자 상세 및 수정</title>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link href="<c:url value='/css/style_popup.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/fonts/font-awesome/css/font-awesome.min.css' />" rel="stylesheet" />
<link href="<c:url value='/css/ax_page.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/css/AXJ.min.css' />" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<c:url value='/js/lib/jquery-1.12.4.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/lib/jquery.number.js' />" ></script>

<script type="text/javascript" src="<c:url value='/js/pms/cmm.js' />" ></script> 
<script type="text/javascript" src="<c:url value='/js/pms/pms.js' />" ></script> 


<script type="text/javaScript" language="javascript" defer="defer">
<!--
var chk = false; //중복확인


$(document).ready(function() {
	
	gfn_init();
	
	//회사 콤보 공통코드
	gfn_setCombo("ofcCd", "OFCE", "${usrDtl.ofcCd}");
	//부서 콤보 공통코드
	gfn_setCombo("deptCd", "DEPT", "${usrDtl.deptCd}");
	//직급 콤보 공통코드
	gfn_setCombo("posCd", "POSI", "${usrDtl.posCd}");
	//권한 콤보 공통코드
	gfn_setCombo("authCd", "ROLE", "${usrDtl.authCd}");
	
	
	// 버튼권한처리
	setBtnAuth();
	
});


//저장
function fnUpdate(){
    //if(validateUserManageVO(document.frm)){
    //}
    //document.frm.submit();
    
    if(!gfn_formValid("frm"))	return;
    
	var regEng = /^[A-Za-z0-9_]*$/;//영문숫자 정규식
	
	if(!regEng.test($("#userId").val())){
		alert("ID는 영문,숫자로 입력하세요.");
		$("#userId").focus();
		return;
	}
	if($("#userPwd").val() != $("#userPwd2").val()){
		alert("비밀번호 확인값이 다릅니다.","",function(){
			$("#userPwd2").focus();
		});
		return;
	}
	if(!gfn_isTel($("#userTel").val())){
		alert("잘못된 전화번호  형식입니다.","",function(){
			$("#userTel").focus();
		});
		return;
	}
	
	if(!chk && "${mode}" == "ADD"){
		alert("중복확인을 확인하세요.");
		return;
	}

	var useYn = $("input:radio[name='useYn']:checked").val();
    var param = {};
    param.formId = "frm"; 
    param.useYn = useYn;
    
	if(!confirm("저장하시겠습니까?")) return;
	debugger;
	gfn_saveForm("frm", "/pmis/pms/adm/saveUser.do" , function(data){
		
		if(data.result){
			alert("데이터가 저장되었습니다.");
			if("${mode}" == "ADD"){
				opener.fnSearch();
				self.close();
			}
			else{
				fnRefresh();
			}
		}else{
			alert("데이터 저장에 실패하였습니다. : " + data.error,"E");
		}
	});
    
}


/// 중복체크
function doCheck() {
	
	if(gfn_isNull($("#userId").val())){
		alert("먼저 ID를 입력하세요.");
		$("#userId").focus();
		return;
	}
	
	var regEng = /^[A-Za-z0-9_]*$/;//영문숫자 정규식
	
	if(!regEng.test($("#userId").val())){
		alert("ID는 영문,숫자로 입력하세요.");
		$("#userId").focus();
		return;
	}
	
	gfn_selectList({
		sqlId : "selectUserDtl",
		userId:$("#userId").val()}
	
	, function(data){
		if(data.data.length > 0){
			alert("중복된 ID입니다.");
			chk = false;
			$("#btnChk").attr("disabled",false);
			$("#userId").removeAttr("readonly");				
			return;
		}else{
			alert("사용가능한 ID입니다.");
			chk = true;
			$("#btnChk").attr("disabled",true);
			$("#userId").attr("readonly","readonly");				
		}
	} );		
	
}


/// 버튼및권한처리
var setBtnAuth = function(){
	
	switch("${mode}"){
	case "ADD" :
		$("#userId").attr("readonly",false);				
		$("#btnChk").attr("disabled",false);
		$("#userPwd").attr("disabled",false);
		$("#userPwd2").attr("disabled",false);
		$("#btnPwd").hide();			
		break;
		
	case "MOD" :		
		$("#userId").attr("readonly",true);				
		$("#btnChk").attr("disabled",true);
		$("#userPwd").attr("disabled",true);				
		$("#userPwd2").attr("disabled",true);
		if("${sessionScope.isAdmYn}" == "Y" || "${sessionScope.id}" == "${usrDtl.userId}"){
			$("#btnPwd").show();
		}
		break;
		
	default :	
		break;
	};
	
	
};


function fnRefresh(){
    document.frm.action = "<c:url value='/pms/adm/usrUpdt.do'/>";
    document.frm.submit();
}


//비밀번호변경모드
var doPwd = function(){
	$("#CHG_PWD").val("Y");
	$("#userPwd").attr("disabled",false);
	$("#userPwd2").attr("disabled",false);
};




//-->
</script>

</head>
<body>
<noscript>자바스크립트를 지원하지 않는 브라우저에서는 일부 기능을 사용하실 수 없습니다.</noscript>    

<!-- UI Object -->
<div id="wrap"> 
  
 
  
<h1><i class="fa fa-chevron-right  "></i> 사용자정보</h1>
<div class="conts"  >
          
          
 	<form id="frm" name="frm" >
		<input type="hidden" id="mode" name="mode" value="${mode}"/>
		<input type="hidden" id="CHG_PWD" name="CHG_PWD" value="N"/>
           
    	<!-- write -->
          
          <div class="table-responsive">
            <table class="table table-write">
              <caption>사용자정보
              </caption>
              <colgroup>
              <col width="30%">
              <col width="70%">
              </colgroup>
              <tbody>
                <tr>
                  <th class="req">아이디</th>
                  <td><input type="text" class="form-control reqVal" style="width:68.5%"  name="userId"  id="userId" value="${usrDtl.userId}">
                  		<button type="button" id="btnChk" class="AXButton" style="width:20%;" onclick="doCheck();"  ><i class="fa fa-lg"></i> 중복확인</button>
                  </td>
                </tr>
                <tr>
                  <th class="req">이름</th>
                  <td><input type="text" class="form-control reqVal" style="width:98.5%"  name="userNm" value="${usrDtl.userNm}" ></td>
                </tr>
                <tr>
                  <th>연락처</th>
                  <td><input type="text" class="form-control" style="width:98.5%"  name="userTel" id="userTel" value="${usrDtl.userTel}" ></td>
                </tr>
                <tr>
                  <th class="req">비밀번호</th>
                  <td><input type="password" class="form-control reqVal" name="userPwd" id="userPwd" value="${usrDtl.userPwd}" style="width:98.5%"  ></td>
                </tr>
                <tr>
                  <th>비밀번호 확인</th>
                  <td><input type="password" class="form-control" id="userPwd2" value="${usrDtl.userPwd}" style="width:98.5%"  ></td>
                </tr>
                <tr>
                  <th>회사</th>
                  <td>
                    <select class="form-control input-inline" style="width:400px;"  name="ofcCd"  id="ofcCd"  style="width:98.5%"  >	                    
                    </select>
                  </td>
                </tr>
                <tr>
                  <th>부서</th>
                  <td>
                    <select class="form-control input-inline" style="width:400px;"  name="deptCd" id="deptCd"  style="width:98.5%"  >	                    
                    </select>
                  </td>
                </tr>
                <tr>
                  <th>직급</th>
                  <td>
                    <select class="form-control input-inline" style="width:400px;"  name="posCd" id="posCd"  style="width:98.5%"  >	                    
                    </select>
                  </td>
                </tr>
                <tr>
                <tr>
                  <th>권한</th>
                  <td>
                    <select class="form-control input-inline" style="width:400px;"  name="authCd" id="authCd"  style="width:98.5%" >	                    
                    </select>
                  </td>
                </tr>
                <tr>
                  <th>비고</th>
                  <td><input type="text" class="form-control" value="${usrDtl.etc}" name="etc" style="width:98.5%"  ></td>
                </tr>
                <tr>
                  <th>사용여부</th>
                  <td>
                  	<input name="useYn" type="radio" class="i_radio" id="c1" value="Y" <c:if test="${empty usrDtl.useYn ||  usrDtl.useYn == 'Y'}"> checked</c:if> >
                    <label for="c1"> 사용</label>
                    <input name="useYn" type="radio" class="i_radio" id="c2" value="N"  <c:if test="${usrDtl.useYn == 'N'}"> checked</c:if> >
                    <label for="c2"> 미사용</label></td>
                </tr>
              </tbody>
            </table>
          </div>
    <div class="btn_group center m-t-15 "  >
     
       
      <button id="btnSave" class="btn btn-default" onclick="JavaScript:fnUpdate(); return false;"> <i class="fa fa-check" aria-hidden="true"></i> 저장</button>
      <button id="btnPwd" class="btn btn-default" onclick="JavaScript:doPwd(); return false;"> <i class="fa fa-check" aria-hidden="true"></i> 비밀번호변경</button>
       <button id="button" class="btn btn-default " onclick="JavaScript:self.close();"> <i class="fa fa-close" aria-hidden="true"></i> 닫기</button>
     </div>
          
          <!-- //write --> 
    
     </form>
     
  </div>
  <!--conts--> 
  
   </div>

</body>
</html>

