<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko">
<head>
<title>사후관리시스템</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link href="<c:url value='/css/style.css' />" rel="stylesheet" type="text/css">
<link href="<c:url value='/fonts/font-awesome/css/font-awesome.min.css' />" rel="stylesheet" />

<script type="text/javascript" src="<c:url value='/js/lib/jquery-1.12.4.js' />" ></script>
<script type="text/javascript" src="<c:url value='/js/pms/cmm.js' />" ></script> 
<script type="text/javascript">
<!--
$(document).ready(function() {
	
	//서버메세지체크
    var message = document.loginForm.message.value;
    if (!gfn_isNull(message)) {
        alert(message);
    }
    //getid(document.loginForm);

    
    $("#id").focus();
});

function actionLogin() {

    if (document.loginForm.id.value =="") {
        alert("아이디를 입력하세요");
        return false;
    } else if (document.loginForm.password.value =="") {
        alert("비밀번호를 입력하세요");
        return false;
    } else {
        document.loginForm.action="<c:url value='/uat/uia/actionSecurityLogin.do'/>";
        //document.loginForm.j_username.value = document.loginForm.userSe.value + document.loginForm.username.value;
        //document.loginForm.action="<c:url value='/j_spring_security_check'/>";
        document.loginForm.submit();
    }
    
}

function setCookie (name, value, expires) {
    document.cookie = name + "=" + escape (value) + "; path=/; expires=" + expires.toGMTString();
}

function getCookie(Name) {
    var search = Name + "="
    if (document.cookie.length > 0) { // 쿠키가 설정되어 있다면
        offset = document.cookie.indexOf(search)
        if (offset != -1) { // 쿠키가 존재하면
            offset += search.length
            // set index of beginning of value
            end = document.cookie.indexOf(";", offset)
            // 쿠키 값의 마지막 위치 인덱스 번호 설정
            if (end == -1)
                end = document.cookie.length
            return unescape(document.cookie.substring(offset, end))
        }
    }
    return "";
}

function saveid(form) {
    var expdate = new Date();
    // 기본적으로 30일동안 기억하게 함. 일수를 조절하려면 * 30에서 숫자를 조절하면 됨
    if (form.checkId.checked)
        expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 30); // 30일
    else
        expdate.setTime(expdate.getTime() - 1); // 쿠키 삭제조건
    setCookie("saveid", form.id.value, expdate);
}

function getid(form) {
    form.checkId.checked = ((form.id.value = getCookie("saveid")) != "");
}

//-->
</script>
</head>

<body class="intro-body">

<form:form id="loginForm" name="loginForm" method="post">

<div class="login-wrap">
    <div class="login-content"  >
      <div class="login-group" > 
        <p style="text-align:center; padding-bottom:20px"> <img src="/pmis/images/ci_login.png"  alt="사후관리시스템"></p>
        <div class="login-box" >
          <table class="table table-login">
            <colgroup>
            <col width="70px">
            <col width="200px">
            <col width="80px">
            </colgroup>
            <tbody>
              <tr>
                <th>아이디</th>
                <td>
                	<input type="text" class="form-control" value="" style="width:190px" id="id" name="userId" >
                </td>
                <td rowspan="2">
                	<button id="button" class="btn btn-login " onclick="javascript:actionLogin()">
                  		<p><i class="fa fa-lock " aria-hidden="true"></i></p>로그인
                	</button>
          		</td>
              </tr>
              <tr>
                <th>비밀번호</th>
                <td>
                    <input type="password" class="form-control" maxlength="25" title="비밀번호를 입력하세요." id="password" name="userPwd" style="width:190px" 
                       onkeydown="javascript:if (event.keyCode == 13) { actionLogin(); }"/>
                	
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>

<input type="hidden" name="message" value="${message}" />
<input type="hidden" name="userSe"  value="USR"/>
<input name="j_username" type="hidden"/>
</form:form>

</body>
</html>