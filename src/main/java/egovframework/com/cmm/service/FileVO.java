package egovframework.com.cmm.service;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @Class Name : FileVO.java
 * @Description : 파일정보 처리를 위한 VO 클래스
 * @Modification Information
 *
 *    수정일       수정자         수정내용
 *    -------        -------     -------------------
 *    2009. 3. 25.     이삼섭
 *
 * @author 공통 서비스 개발팀 이삼섭
 * @since 2009. 3. 25.
 * @version
 * @see
 *
 */
@SuppressWarnings("serial")
public class FileVO implements Serializable {

    /**
     * 첨부파일 아이디
     */
    public String atchFileId = "";
    /**
     * 생성일자
     */
    public String creatDt = "";
    /**
     * 파일내용
     */
    public String fileCn = "";
    /**
     * 파일확장자
     */
    public String fileExtsn = "";
    /**
     * 파일크기
     */
    public String fileMg = "";
    /**
     * 파일연번
     */
    public String fileSn = "";
    /**
     * 파일저장경로
     */
    public String fileStreCours = "";
    /**
     * 원파일명
     */
    public String orignlFileNm = "";
    /**
     * 저장파일명
     */
    public String streFileNm = "";

    /**
     * 파일순번
     */
    public String fileSeq = "";
    /**
     * 자료실구분
     */
    public String catCd = "";
    
    /**
     * 순번
     */
    public String seq = "";
    
    /**
     * 파일경로
     */
    public String filePath = "";
    /**
     * 파일크기
     */
    public String fileSize = "";
    /**
     * 등록일자
     */
    public String edtDt = "";
    /**
     * 등록자
     */
    public String edtId = "";
    
    /**
     * 파일크기
     */
    public String fileSizeMb = "";

    /**
     * fileSeq attribute를 리턴한다.
     * 
     * @return the fileSeq
     */
    public String getFileSeq() {
    	return fileSeq;
    }
    /**
     * 등록자
     */
    public String fileSort = "";

    /**
     * fileSeq attribute 값을 설정한다.
     * 
     * @param fileSeq
     *            the fileSeq to set
     */
    public void setFileSeq(String fileSeq) {
    	this.fileSeq = fileSeq;
    }
    

    /**
     * catCd attribute를 리턴한다.
     * 
     * @return the fileSeq
     */
    public String getCatCd() {
    	return catCd;
    }

    /**
     * catCd attribute 값을 설정한다.
     * 
     * @param catCd
     *            the fileSeq to set
     */
    public void setCatCd(String catCd) {
    	this.fileSeq = catCd;
    }

    /**
     * seq attribute를 리턴한다.
     * 
     * @return the seq
     */
    public String getSeq() {
    	return seq;
    }

    /**
     * Seq attribute 값을 설정한다.
     * 
     * @param seq
     *            the fileSeq to set
     */
    public void setSeq(String seq) {
    	this.seq = seq;
    }

    /**
     * filePath attribute를 리턴한다.
     * 
     * @return the filePath
     */
    public String getFilePath() {
    	return filePath;
    }

    /**
     * fileSeq attribute 값을 설정한다.
     * 
     * @param filePath
     *            the fileSeq to set
     */
    public void setFilePath(String filePath) {
    	this.filePath = filePath;
    }    

    /**
     * fileSize attribute를 리턴한다.
     * 
     * @return the fileSize
     */
    public String getFileSize() {
    	return fileSize;
    }

    /**
     * fileSize attribute 값을 설정한다.
     * 
     * @param fileSize
     *            the fileSeq to set
     */
    public void setFileSize(String fileSize) {
    	this.fileSize = fileSize;
    }    
    

    /**
     * fileSize attribute를 리턴한다.
     * 
     * @return the fileSize
     */
    public String getFileSizeMb() {
    	return fileSizeMb;
    }

    /**
     * fileSize attribute 값을 설정한다.
     * 
     * @param fileSize
     *            the fileSeq to set
     */
    public void setFileSizeMb(String fileSizeMb) {
    	this.fileSizeMb = fileSizeMb;
    }    

    /**
     * edtId attribute를 리턴한다.
     * 
     * @return the fileSeq
     */
    public String getEdtId() {
    	return edtId;
    }

    /**
     * edtId attribute 값을 설정한다.
     * 
     * @param catCd
     *            the fileSeq to set
     */
    public void setEdtId(String edtId) {
    	this.edtId = edtId;
    }

    /**
     * edtDt attribute를 리턴한다.
     * 
     * @return the fileSeq
     */
    public String getEdtDt() {
    	return edtDt;
    }

    /**
     * edtDt attribute 값을 설정한다.
     * 
     * @param catCd
     *            the fileSeq to set
     */
    public void setEdtDt(String edtDt) {
    	this.edtDt = edtDt;
    }

    /**
     * fileSort attribute를 리턴한다.
     * 
     * @return the fileSeq
     */
    public String getFileSort() {
    	return fileSort;
    }

    /**
     * fileSort attribute 값을 설정한다.
     * 
     * @param catCd
     *            the fileSeq to set
     */
    public void setFileSort(String fileSort) {
    	this.fileSort = fileSort;
    }
    
    
    /**
     * atchFileId attribute를 리턴한다.
     * 
     * @return the atchFileId
     */
    public String getAtchFileId() {
	return atchFileId;
    }

    /**
     * atchFileId attribute 값을 설정한다.
     * 
     * @param atchFileId
     *            the atchFileId to set
     */
    public void setAtchFileId(String atchFileId) {
	this.atchFileId = atchFileId;
    }

    /**
     * creatDt attribute를 리턴한다.
     * 
     * @return the creatDt
     */
    public String getCreatDt() {
	return creatDt;
    }

    /**
     * creatDt attribute 값을 설정한다.
     * 
     * @param creatDt
     *            the creatDt to set
     */
    public void setCreatDt(String creatDt) {
	this.creatDt = creatDt;
    }

    /**
     * fileCn attribute를 리턴한다.
     * 
     * @return the fileCn
     */
    public String getFileCn() {
	return fileCn;
    }

    /**
     * fileCn attribute 값을 설정한다.
     * 
     * @param fileCn
     *            the fileCn to set
     */
    public void setFileCn(String fileCn) {
	this.fileCn = fileCn;
    }

    /**
     * fileExtsn attribute를 리턴한다.
     * 
     * @return the fileExtsn
     */
    public String getFileExtsn() {
	return fileExtsn;
    }

    /**
     * fileExtsn attribute 값을 설정한다.
     * 
     * @param fileExtsn
     *            the fileExtsn to set
     */
    public void setFileExtsn(String fileExtsn) {
	this.fileExtsn = fileExtsn;
    }

    /**
     * fileMg attribute를 리턴한다.
     * 
     * @return the fileMg
     */
    public String getFileMg() {
	return fileMg;
    }

    /**
     * fileMg attribute 값을 설정한다.
     * 
     * @param fileMg
     *            the fileMg to set
     */
    public void setFileMg(String fileMg) {
	this.fileMg = fileMg;
    }

    /**
     * fileSn attribute를 리턴한다.
     * 
     * @return the fileSn
     */
    public String getFileSn() {
	return fileSn;
    }

    /**
     * fileSn attribute 값을 설정한다.
     * 
     * @param fileSn
     *            the fileSn to set
     */
    public void setFileSn(String fileSn) {
	this.fileSn = fileSn;
    }

    /**
     * fileStreCours attribute를 리턴한다.
     * 
     * @return the fileStreCours
     */
    public String getFileStreCours() {
	return fileStreCours;
    }

    /**
     * fileStreCours attribute 값을 설정한다.
     * 
     * @param fileStreCours
     *            the fileStreCours to set
     */
    public void setFileStreCours(String fileStreCours) {
	this.fileStreCours = fileStreCours;
    }

    /**
     * orignlFileNm attribute를 리턴한다.
     * 
     * @return the orignlFileNm
     */
    public String getOrignlFileNm() {
	return orignlFileNm;
    }

    /**
     * orignlFileNm attribute 값을 설정한다.
     * 
     * @param orignlFileNm
     *            the orignlFileNm to set
     */
    public void setOrignlFileNm(String orignlFileNm) {
	this.orignlFileNm = orignlFileNm;
    }

    /**
     * streFileNm attribute를 리턴한다.
     * 
     * @return the streFileNm
     */
    public String getStreFileNm() {
	return streFileNm;
    }

    /**
     * streFileNm attribute 값을 설정한다.
     * 
     * @param streFileNm
     *            the streFileNm to set
     */
    public void setStreFileNm(String streFileNm) {
	this.streFileNm = streFileNm;
    }

    /**
     * toString 메소드를 대치한다.
     */
    public String toString() {
	return ToStringBuilder.reflectionToString(this);
    }
	
}
