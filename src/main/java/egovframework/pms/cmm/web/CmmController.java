package egovframework.pms.cmm.web;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.util.JsonUtils;

/**
 * 사업관리 
 * @author 공통서비스 개발팀 
 * @see
 *
 */
@Controller
public class CmmController {


	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;

	@Resource(name = "jsonUtils")
	private JsonUtils jsonUtils;

	
	
	

	
	/**
	 * 공통조회  - ajax
	 * @param request
	 * @param commandMap : sqlId - 쿼리아이디, sqlId2 - 쿼리아이디2 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cmm/selectList.do")
	public ModelAndView selectList(HttpServletRequest request, CommandMap commandMap) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");
		Map<String, Object> map = commandMap.getMap();
		
		JSONObject result = new JSONObject();
		
		try {
			result.put("result", true);
			result.put("data", jsonUtils.List2JSONArr((List<Map<String, Object>>) cmmService.selectList(map.get("sqlId").toString(), map)));
			
			// 다른 조회건이 있으면 data2로 보낸다
			if(map.get("sqlId2") != null && !"".equals(map.get("sqlId2").toString()) ){
				result.put("data2", jsonUtils.List2JSONArr((List<Map<String, Object>>) cmmService.selectList(map.get("sqlId2").toString(), map)));
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			result.put("result", false);
			result.put("error", e.getMessage());
		}
		
		
		
		mv.addObject("result", result.toJSONString());
		
		return mv;
	}
	
	
	
	
	
	/**
	 * ajax 단건 공통저장처리
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/upd.do")
	@ResponseBody
	public ModelAndView upd(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");


		String result = cmmService.saveData(param);
		
		mv.addObject("result", result);
		return mv;
	}
	
	/**
	 * ajax 리스트 공통저장처리
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/updList.do")
	@ResponseBody
	public ModelAndView updList(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");
		
		
		String result = cmmService.saveCmmList(param);
		
		mv.addObject("result", result);
		return mv;
	}
	
	
	
	/**
	 * ajax 단건 form 데이터 공통저장처리
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/formUpd.do")
	public ModelAndView upd(CommandMap commandMap, HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		String result = cmmService.saveFormData(commandMap.getMap());
		mv.addObject("result", result);

		return mv;
	}
	
	
}
