package egovframework.pms.cmm.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component("stringUtils")
public class StringUtils {

	public boolean isNumber(Object obj) {

		boolean result = false;

		try {

			String str = obj.toString();
			Double.parseDouble(str);
			result = true;

		} catch (Exception e) {
		}

		return result;
	}

	public String cutStr(Object obj, int cutByte) throws Exception {

		return cutStr(replaceEmptyStr(obj, ""), cutByte);
	}

	public String cutStr(String str, int cutByte) throws Exception {

		byte[] strByte = str.getBytes();
		if (strByte.length < cutByte)
			return str;
		int cnt = 0;
		for (int i = 0; i < cutByte; i++) {
			if (strByte[i] < 0)
				cnt++;
		}

		String r_str;
		if (cnt % 2 == 0)
			r_str = new String(strByte, 0, cutByte);
		else
			r_str = new String(strByte, 0, cutByte - 1);

		return r_str;
	}

	public String cutStr(String str, int idx, int len) throws Exception {

		String r_str = "";

		if (str.length() >= idx) {
			r_str = str.substring(idx, str.length() < (idx + len) ? str.length() : (idx + len));
		}

		return r_str;
	}

	public int getSpcStrCnt(String pattern_str, String str) throws Exception {

		int cnt = 0;

		Pattern p = Pattern.compile(pattern_str);
		Matcher m = p.matcher(str);
		for (int i = 0; m.find(i); i = m.end()) {
			cnt++;
		}

		return cnt;
	}

	public String replaceEmptyStr(String str, String replace_str) throws Exception {

		if (str == null || "".equals(str.trim())) {
			str = replace_str;
		}

		return str.trim();
	}

	public String replaceEmptyStr(Object obj, String replace_str) throws Exception {

		String str = "";

		if (obj == null || "".equals(obj.toString().trim())) {
			str = replace_str;
		} else {
			str = obj.toString();
		}

		return str.trim();
	}

	public String str2num(String str, int scale) throws Exception {

		BigDecimal bd = BigDecimal.valueOf(0);

		try {

			String t_str = str.replaceAll("[^\\d\\.]", "");

			bd = new BigDecimal(t_str);

		} catch (Exception e) {

		}

		return String.valueOf(bd.setScale(scale, BigDecimal.ROUND_HALF_UP));
	}

	public String obj2num(Object obj, int scale) throws Exception {

		BigDecimal bd = BigDecimal.valueOf(0);

		try {

			String t_str = obj.toString().replaceAll("[^\\d\\.]", "");

			bd = new BigDecimal(t_str);

		} catch (Exception e) {

		}

		return String.valueOf(bd.setScale(scale, BigDecimal.ROUND_HALF_UP));
	}

	public String removeSpcTxt(String str) throws Exception {

		try {

			str = str.replaceAll("\\[", "(");
			str = str.replaceAll("\\]", ")");
			str = str.replaceAll("@", "");
			str = str.replaceAll("#", "");

		} catch (Exception e) {

		}

		return str.trim();
	}

	public String removeSpcTxt2(String str) throws Exception {

		try {

			str = str.replaceAll("\\[", " ");
			str = str.replaceAll("\\]", " ");
			str = str.replaceAll("@", " ");
			str = str.replaceAll("#", " ");
			str = str.replaceAll(",", " ");
			while (str.indexOf("  ") != -1) {
				str = str.replaceAll("  ", " ");
			}

		} catch (Exception e) {

		}

		return str.trim();
	}

	public String toBlank(String tg) {
		String result = "";

		if (tg != null) {
			result = "" + tg;
		}

		return result.trim();
	}

	public String toBlank(Object tg) {
		String result = "";

		if (tg != null) {
			result = "" + tg;
		}

		return result.trim();
	}

	public String toBlankZ(String tg) {
		String result = "0";

		if (tg != null && !tg.trim().equals("")) {
			result = "" + tg;
			result = result.trim().equals("0") || result.trim().equals("0.00") ? "0" : result;
		}

		return result.trim();
	}

	public String toBlankZ(Object tg) {
		String result = "";

		if (tg != null) {
			result = toBlankZ("" + tg);
		}

		return result.trim();
	}

	public double toDouble(String target) {
		double result = 0.0;

		if (target != null && !target.trim().equals("") && !target.trim().toUpperCase().equals("NULL")) {
			target = target.replaceAll(",", "").replaceAll(" ", "");
			result = Double.parseDouble(target);
		}

		return result;
	}

	public double toDouble(Object target) {
		double result = 0.0;

		if (target != null) {
			result = toDouble("" + target);
		}

		return result;
	}

	public String cutSosu(String target, int len, String type, String comma) {
		double num = toDouble(target);
		DecimalFormat df = null;

		if (comma != null && comma.toUpperCase().equals("Y")) {
			// õ���� �޸�

			if (type != null && type.toUpperCase().equals("Y")) {
				// �Ҽ��� �ڸ� ����

				if (len == 1) {
					df = new DecimalFormat("#,##0.0");
				} else if (len == 2) {
					df = new DecimalFormat("#,##0.00");
				} else if (len == 3) {
					df = new DecimalFormat("#,##0.000");
				} else if (len == 4) {
					df = new DecimalFormat("#,##0.0000");
				} else {
					df = new DecimalFormat("#,##0");
				}

			} else {
				// �Ҽ��� �ڸ� ����

				if (len == 1) {
					df = new DecimalFormat("#,##0.#");
				} else if (len == 2) {
					df = new DecimalFormat("#,##0.##");
				} else if (len == 3) {
					df = new DecimalFormat("#,##0.###");
				} else if (len == 4) {
					df = new DecimalFormat("#,##0.####");
				} else {
					df = new DecimalFormat("#,##0");
				}

			}
		} else {
			// ���� ǥ��

			if (type != null && type.toUpperCase().equals("Y")) {
				// �Ҽ��� �ڸ� ����

				if (len == 1) {
					df = new DecimalFormat("###0.0");
				} else if (len == 2) {
					df = new DecimalFormat("###0.00");
				} else if (len == 3) {
					df = new DecimalFormat("###0.000");
				} else if (len == 4) {
					df = new DecimalFormat("###0.0000");
				} else {
					df = new DecimalFormat("###0");
				}

			} else {
				// �Ҽ��� �ڸ� ����

				if (len == 1) {
					df = new DecimalFormat("###0.#");
				} else if (len == 2) {
					df = new DecimalFormat("###0.##");
				} else if (len == 3) {
					df = new DecimalFormat("###0.###");
				} else if (len == 4) {
					df = new DecimalFormat("###0.####");
				} else {
					df = new DecimalFormat("###0");
				}

			}
		}

		return df.format(num);
	}

	public String cutSosu(Object target, int len, String type, String comma) {
		return cutSosu("" + target, len, type, comma);
	}

	public String cutSosu(Double target, int len, String type, String comma) {
		return cutSosu("" + target, len, type, comma);
	}

	/**
	 * @Method_Name	: reduceSpace
	 * @�ۼ���		: 2017. 3. 24. 
	 * @�ۼ���		: ������
	 * @Method_����	: �������� ���鹮��(Space)�� �Ѱ��� ����
	 * @param str
	 * @return String
	 */
	public String reduceSpace(String str) {

		while (str.indexOf("  ") > -1) {
			str = str.replaceAll("  ", " ");
		}

		return str;
	}

	/**
	 * @Method_Name	: reduceSpace
	 * @�ۼ���		: 2017. 3. 24. 
	 * @�ۼ���		: ������
	 * @Method_����	: �������� ���鹮��(Space)�� �Ѱ��� ����
	 * @param obj
	 * @return String
	 */
	public String reduceSpace(Object obj) {

		return reduceSpace(String.valueOf(obj));
	}

	/**
	 * @Method_Name	: restrictAlpha
	 * @�ۼ���		: 2017. 3. 31. 
	 * @�ۼ���		: ������
	 * @Method_����	: Alphabet �� ���ڿ� ����
	 * @param str
	 * @return String
	 */
	public String restrictAlpha(String str) {
		
		return str.replaceAll("[^A-Za-z]", "");
	}

	/**
	 * @Method_Name	: restrictAlpha
	 * @�ۼ���		: 2017. 3. 31. 
	 * @�ۼ���		: ������
	 * @Method_����	: Alphabet �� ���ڿ� ����
	 * @param obj
	 * @return String
	 */
	public String restrictAlpha(Object obj) {
		
		return String.valueOf(obj).replaceAll("[^A-Za-z]", "");
	}

	/**
	 * @Method_Name	: restrictAlphaNum
	 * @�ۼ���		: 2017. 3. 31. 
	 * @�ۼ���		: ������
	 * @Method_����	: Alphabet, Numeric �� ���ڿ� ����
	 * @param str
	 * @return String
	 */
	public String restrictAlphaNum(String str) {
		
		return str.replaceAll("[^A-Za-z0-9]", "");
	}

	/**
	 * @Method_Name	: restrictAlphaNum
	 * @�ۼ���		: 2017. 3. 31. 
	 * @�ۼ���		: ������
	 * @Method_����	: Alphabet, Numeric �� ���ڿ� ����
	 * @param obj
	 * @return String
	 */
	public String restrictAlphaNum(Object obj) {
		
		return String.valueOf(obj).replaceAll("[^A-Za-z0-9]", "");
	}

	/**
	 * @Method_Name	: restrictAlphaWithSpace
	 * @�ۼ���		: 2017. 3. 31. 
	 * @�ۼ���		: ������
	 * @Method_����	: Alphabet, Space �� ���ڿ� ����
	 * @param str
	 * @return String
	 */
	public String restrictAlphaWithSpace(String str) {
		
		return str.replaceAll("[^A-Za-z ]", "");
	}

	/**
	 * @Method_Name	: restrictAlphaWithSpace
	 * @�ۼ���		: 2017. 3. 31. 
	 * @�ۼ���		: ������
	 * @Method_����	: Alphabet, Space �� ���ڿ� ����
	 * @param obj
	 * @return String
	 */
	public String restrictAlphaWithSpace(Object obj) {
		
		return String.valueOf(obj).replaceAll("[^A-Za-z ]", "");
	}

	/**
	 * @Method_Name	: restrictAlphaNumWithSpace
	 * @�ۼ���		: 2017. 3. 31. 
	 * @�ۼ���		: ������
	 * @Method_����	: Alphabet, Numeric, Space �� ���ڿ� ����
	 * @param str
	 * @return String
	 */
	public String restrictAlphaNumWithSpace(String str) {
		
		return str.replaceAll("[^A-Za-z0-9 ]", "");
	}

	/**
	 * @Method_Name	: restrictAlphaNumWithSpace
	 * @�ۼ���		: 2017. 3. 31. 
	 * @�ۼ���		: ������
	 * @Method_����	: Alphabet, Numeric, Space �� ���ڿ� ����
	 * @param obj
	 * @return String
	 */
	public String restrictAlphaNumWithSpace(Object obj) {
		
		return String.valueOf(obj).replaceAll("[^A-Za-z0-9 ]", "");
	}

	/**
	 * @Method_Name	: convOCRNumTxt
	 * @�ۼ���		: 2017. 5. 10. 
	 * @�ۼ���		: ������
	 * @Method_����	: OCR�� �������� �ؽ�Ʈ ����
	 * @param str
	 * @return String
	 */
	public String revOCRNumTxt(String str) {

		str = str.replaceAll("a", "8")
				 .replaceAll("A", "4")
				 .replaceAll("B", "8")
				 .replaceAll("C", "0")
				 .replaceAll("G", "6")
				 .replaceAll("l", "1")
				 .replaceAll("I", "1")
				 .replaceAll("O", "0")
				 .replaceAll("z", "2")
				 .replaceAll("Z", "2");

		return str;
	}
}
