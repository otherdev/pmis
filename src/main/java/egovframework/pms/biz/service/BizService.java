package egovframework.pms.biz.service;
import java.util.List;
import java.util.Map;

/**
 * 사업관리에 관한 인터페이스클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 *
 * </pre>
 */
public interface BizService  {

	


	public List<?> selectBizList(BizVO bizVO) throws Exception;

	/**
	 * 사업관리목록의 전체수를 확인
	 * @param bizVO 검색조건
	 * @return 총사용자갯수(int)
	 * @throws Exception
	 */
	public int selectBizListTotCnt(BizVO bizVO) throws Exception;

	/**
	 * 화면에 조회된 사용자의 기본정보를 수정하여 항목의 정합성을 체크하고 수정된 데이터를 데이터베이스에 반영
	 * @param userManageVO 업무사용자 수정정보
	 * @throws Exception
	 */
	public BizModVO selectBiz(BizVO bizVO);
	public void updateBiz(BizModVO bizModVO) throws Exception;
	String saveSchdList(Map<String,Object> param)throws Exception;

	public List<?> selectWorkList(BizVO bizVO) throws Exception;
	public int selectWorkListTotCnt(BizVO bizVO) throws Exception;
	public BizModVO selectWork(BizVO bizVO);
	public void updateWork(BizModVO bizModVO) throws Exception;

	String saveChgMst(Map<String, Object> map) throws Exception;



}