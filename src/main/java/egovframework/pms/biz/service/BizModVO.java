package egovframework.pms.biz.service;

/**
 * 사업관리VO클래스로서 사업관리관리 비지니스로직 처리용 항목을 구성한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *   
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2017.04.10           최초 생성
 *
 * </pre>
 */
public class BizModVO extends BizVO{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/** */
	private String 	ttl	 = "";
	private String 	contents	 = "";
	private String 	workStartDt	 = "";
	private String 	workEndDt	 = "";
	private String 	schedStartDt	 = "";
	private String 	schedEndDt	 = "";
	private long 	runMpw	 = 0L;
	private long 	planMpw	 = 0L;
	private long 	planRate	 = 0L;
	private String 	delYn	 = "";
	private String 	regId	 = "";
	private String 	regDt	 = "";
	private String 	edtId	 = "";
	private String 	edtDt	 = "";
	private String 	aprvId	 = "";
	private String 	aprvDt	 = "";
	private String 	aprvYn	 = "";

	private long 	bizCost	 = 0L;
	private String 	bizCatNm	 = "";
	private String 	bizStartDt	 = "";
	private String 	bizEndDt	 = "";
	private String 	bizDesc	 = "";
	private String 	bizNm	 = "";
	private String 	schedNm	 = "";
	
	private String 	bizTerm	 = "";
	private long 	runRate	 = 0L;
	
	private String 	fileSeq	 = "";
	
	
	
	public String getBizTerm() {
		return bizTerm;
	}
	public void setBizTerm(String bizTerm) {
		this.bizTerm = bizTerm;
	}
	public long getRunRate() {
		return runRate;
	}
	public void setRunRate(long runRate) {
		this.runRate = runRate;
	}
	public long getPlanMpw() {
		return planMpw;
	}
	public void setPlanMpw(long planMpw) {
		this.planMpw = planMpw;
	}
	public String getSchedNm() {
		return schedNm;
	}
	public void setSchedNm(String schedNm) {
		this.schedNm = schedNm;
	}
	private String 	fleRegDt	 = "";
	private String 	fleRegId	 = "";
	
	
	
	
	
	
	
	
	
	public long getPlanRate() {
		return planRate;
	}
	public void setPlanRate(long planRate) {
		this.planRate = planRate;
	}
	public String getSchedStartDt() {
		return schedStartDt;
	}
	public void setSchedStartDt(String schedStartDt) {
		this.schedStartDt = schedStartDt;
	}

	public String getSchedEndDt() {
		return schedEndDt;
	}
	public void setSchedEndDt(String schedEndDt) {
		this.schedEndDt = schedEndDt;
	}
	public String getFleRegDt() {
		return fleRegDt;
	}
	public void setFleRegDt(String fleRegDt) {
		this.fleRegDt = fleRegDt;
	}
	public String getFleRegId() {
		return fleRegId;
	}
	public void setFleRegId(String fleRegId) {
		this.fleRegId = fleRegId;
	}
	private String 	bizCat	 = "";
	private String 	schedCat = "";
	
	private String	BIZ_SEQ	= "";
	private String	BIZ_NM	= "";
	private String	SCHED_SEQ	= "";
	private String	SCHED_NM	= "";

//	private String	WORK_SEQ	= "";
//	private String	SCHED_SEQ	= "";
//	private String	TTL	= "";
//	private String	CONTENTS	= "";
//	private String	WORK_START_DT	= "";
//	private String	WORK_END_DT	= "";
//	private String	RUN_MPW	= "";
//	private String	DEL_YN	= "";
//	private String	REG_ID	= "";
//	private String	REG_DT	= "";
//	private String	EDT_ID	= "";
//	private String	EDT_DT	= "";
//	private String	APRV_ID	= "";
//	private String	APRV_DT	= "";

	
	
	
	
	
	public String getBizCat() {
		return bizCat;
	}
	public String getBizNm() {
		return bizNm;
	}
	public void setBizNm(String bizNm) {
		this.bizNm = bizNm;
	}
	public String getBizDesc() {
		return bizDesc;
	}
	public void setBizDesc(String bizDesc) {
		this.bizDesc = bizDesc;
	}
	public long getBizCost() {
		return bizCost;
	}
	public void setBizCost(long bizCost) {
		this.bizCost = bizCost;
	}
	public String getBizCatNm() {
		return bizCatNm;
	}
	public void setBizCatNm(String bizCatNm) {
		this.bizCatNm = bizCatNm;
	}
	public String getBizStartDt() {
		return bizStartDt;
	}
	public void setBizStartDt(String bizStartDt) {
		this.bizStartDt = bizStartDt;
	}
	public String getBizEndDt() {
		return bizEndDt;
	}
	public void setBizEndDt(String bizEndDt) {
		this.bizEndDt = bizEndDt;
	}
	public void setBizCat(String bizCat) {
		this.bizCat = bizCat;
	}

	public String getTtl() {
		return ttl;
	}
	public void setTtl(String ttl) {
		this.ttl = ttl;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public String getWorkStartDt() {
		return workStartDt;
	}
	public void setWorkStartDt(String workStartDt) {
		this.workStartDt = workStartDt;
	}
	public String getWorkEndDt() {
		return workEndDt;
	}
	public void setWorkEndDt(String workEndDt) {
		this.workEndDt = workEndDt;
	}

	
	public long getRunMpw() {
		return runMpw;
	}
	public void setRunMpw(long runMpw) {
		this.runMpw = runMpw;
	}
	public String getDelYn() {
		return delYn;
	}
	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getEdtId() {
		return edtId;
	}
	public void setEdtId(String edtId) {
		this.edtId = edtId;
	}
	public String getEdtDt() {
		return edtDt;
	}
	public void setEdtDt(String edtDt) {
		this.edtDt = edtDt;
	}
	public String getAprvId() {
		return aprvId;
	}
	public void setAprvId(String aprvId) {
		this.aprvId = aprvId;
	}
	public String getAprvDt() {
		return aprvDt;
	}
	public void setAprvDt(String aprvDt) {
		this.aprvDt = aprvDt;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getSchedCat() {
		return schedCat;
	}
	public void setSchedCat(String schedCat) {
		this.schedCat = schedCat;
	}
	public String getBIZ_SEQ() {
		return BIZ_SEQ;
	}
	public void setBIZ_SEQ(String bIZ_SEQ) {
		BIZ_SEQ = bIZ_SEQ;
	}
	public String getBIZ_NM() {
		return BIZ_NM;
	}
	public void setBIZ_NM(String bIZ_NM) {
		BIZ_NM = bIZ_NM;
	}
	public String getSCHED_SEQ() {
		return SCHED_SEQ;
	}
	public void setSCHED_SEQ(String sCHED_SEQ) {
		SCHED_SEQ = sCHED_SEQ;
	}
	public String getSCHED_NM() {
		return SCHED_NM;
	}
	public void setSCHED_NM(String sCHED_NM) {
		SCHED_NM = sCHED_NM;
	}
	public String getFileSeq() {
		return fileSeq;
	}
	public void setFileSeq(String fileSeq) {
		this.fileSeq = fileSeq;
	}
	public String getAprvYn() {
		return aprvYn;
	}
	public void setAprvYn(String aprvYn) {
		this.aprvYn = aprvYn;
	}
	

		
	
	
}