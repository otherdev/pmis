package egovframework.pms.biz.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import egovframework.com.cmm.SessionUtil;
import egovframework.pms.biz.service.BizModVO;
import egovframework.pms.biz.service.BizService;
import egovframework.pms.biz.service.BizVO;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.service.impl.CmmDAO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;

/**
 * 사용자관리에 관한 비지니스 클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @version 1.0
 * @see
 *
 */
@Service("bizService")
public class BizServiceImpl extends EgovAbstractServiceImpl implements BizService {

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;
	
	
	/** bizDAO */
	@Resource(name="bizDAO")
	private BizDAO bizDAO;

	/** cmmDAO */
	@Resource(name="cmmDAO")
	private CmmDAO cmmDAO;

	/** entrprsManageDAO */
	//EBT-CUSTOMIZING//@Resource(name="entrprsManageDAO")
	//EBT-CUSTOMIZING//private EntrprsManageDAO entrprsManageDAO;

	/** egovUsrCnfrmIdGnrService */
	//	@Resource(name="egovUsrCnfrmIdGnrService")
	//	private EgovIdGnrService idgenService;

	
	
	/**
	 * 사업관리의 정보를 데이터베이스에서 읽어와 화면에 출력
	 * @param bizVO 검색조건
	 * @return List<BizModVO> 업무사용자 목록정보
	 * @throws Exception
	 */
	@Override
	public List<?> selectBizList(BizVO bizVO) {
		List<?> result = bizDAO.selectBizList(bizVO);
		return result;
	}

	/**
	 * 사업관리목록의 전체수를 확인
	 * @param bizVO 검색조건
	 * @return 총사용자갯수(int)
	 * @throws Exception
	 */
	@Override
	public int selectBizListTotCnt(BizVO bizVO) {
		return bizDAO.selectBizListTotCnt(bizVO);
	}	

	@Override
	public BizModVO selectBiz(BizVO bizVO) {
		BizModVO bizModVO = bizDAO.selectBiz(bizVO);
		return bizModVO;
	}
	
	
	@Override
	public void updateBiz(BizModVO bizModVO) throws Exception {
		
		bizModVO.setId(SessionUtil.id);
		bizDAO.updateBiz(bizModVO);
	}
	
	
	@Override
	public String saveSchdList(Map<String,Object> param) throws Exception {
		
		
		// 0.신듀등록이면 채번하고, 사업마스터 등록
		long _bizSeq = Long.parseLong(param.get("bizSeq").toString());
		if(_bizSeq < 0){
			BizModVO vo = (BizModVO) cmmService.select("selectBizSeq", new HashMap<String,Object>());
			_bizSeq = vo.getBizSeq();
			
			//마스터 저장처리 - 임시로 생성
			BizModVO bizModVO = new BizModVO();
			bizModVO.setBizSeq(_bizSeq);
			bizModVO.setBizCat("BIZN01");
			bizModVO.setBizNm("사업명");
			bizModVO.setId(SessionUtil.id);
			bizDAO.update("updateBiz_S",bizModVO);
		}
    	

		
		// 1.공정스케줄  저장
		String sqlId = (String)param.get("sqlId");
		List<Map<String, Object>> lst = (List<Map<String, Object>>) param.get("lst");
		
		int cnt = 0;
		for (Map<String, Object> map : lst) {
			map.put("bizSeq", _bizSeq);//신규등록을 대비해 키 다시 세팅
			map.put("id", SessionUtil.id);
			try{
				int r = cmmDAO.update(sqlId, map);
				if(r>0)	cnt++;
				
			}catch(Exception e){
				throw e;
			}
		}
		
		
		// 2.팀원저장
		String sqlId2 = (String)param.get("sqlId2");
		List<Map<String, Object>> lst2 = (List<Map<String, Object>>) param.get("lst2");
		for (Map<String, Object> map : lst2) {
			map.put("bizSeq", _bizSeq);//신규등록을 대비해 키 다시 세팅
			map.put("id", SessionUtil.id);
			try{
				int r = cmmDAO.update(sqlId2, map);
				if(r>0)	cnt++;
				
			}catch(Exception e){
				throw e;
			}
		}
		
		
		
		
		JSONObject result = new JSONObject();
		result.put("result", true);
		result.put("cnt", cnt);
		result.put("bizSeq", _bizSeq); //신규이건 아니건 사업마스터 키 전달
		return result.toJSONString();
	}
	
	

	/**
	 * 작업관리
	 */
	@Override
	public List<?> selectWorkList(BizVO bizVO) {
		List<?> result = bizDAO.selectWorkList(bizVO);
		return result;
	}
	
	@Override
	public int selectWorkListTotCnt(BizVO bizVO) {
		return bizDAO.selectWorkListTotCnt(bizVO);
	}	
	@Override
	public BizModVO selectWork(BizVO bizVO) {
		BizModVO bizModVO = bizDAO.selectWork(bizVO);
		return bizModVO;
	}
	@Override
	public void updateWork(BizModVO bizModVO) throws Exception {
		bizDAO.updateWork(bizModVO);
	}
	
	
	/**
	 * 공정변경
	 */
	@Override
	public String saveChgMst(Map<String, Object> map) throws Exception {
		
		map.put("id", SessionUtil.id);
		
		int cnt = bizDAO.saveChgMst(map);

		JSONObject result = new JSONObject();
		result.put("result", true);
		result.put("cnt", cnt);
		return result.toJSONString();
	}
	
	

}