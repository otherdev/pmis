package egovframework.pms.biz.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.pms.biz.service.BizModVO;
import egovframework.pms.biz.service.BizVO;
import egovframework.rte.psl.dataaccess.EgovAbstractDAO;

/**
 * 사업관리에 관한 데이터 접근 클래스를 정의한다.
 * @author 공통서비스 개발팀 
 * @since 2017.04.10
 * @version 1.0
 * @see
 *
 */
@Repository("bizDAO")
public class BizDAO extends EgovAbstractDAO{


    /**
     * 사업관리 정보를 데이터베이스에서 읽어와 화면에 출력
     * @param bizVO 검색조건
     * @return List 업무사용자 목록정보
     */
	public List<?> selectBizList(BizVO bizVO){
        return list("selectBizList_S", bizVO);
    }

    /**
     * 총 갯수를 조회한다.
     * @param bizVO 검색조건
     * @return int 업무사용자 총갯수
     */
    public int selectBizListTotCnt(BizVO bizVO) {
        return (Integer)select("selectBizListTotCnt_S", bizVO);
    }
    public BizModVO selectBiz(BizVO bizVO){
    	return (BizModVO) select("selectBiz_S", bizVO);
    }
    public void updateBiz(BizModVO bizModVO){
    	update("updateBiz_S",bizModVO);
    }

    
    
    
    
	public List<?> selectWorkList(BizVO bizVO){
        return list("selectWorkList_S", bizVO);
    }
    public int selectWorkListTotCnt(BizVO bizVO) {
        return (Integer)select("selectWorkListTotCnt_S", bizVO);
    }
    public BizModVO selectWork(BizVO bizVO){
        return (BizModVO) select("selectWork_S", bizVO);
    }
    public void updateWork(BizModVO bizModVO){
        update("updateWork_S",bizModVO);
    }

	public int saveChgMst(Map<String, Object> map) {
        return update("saveChgMst",map);
	}


}