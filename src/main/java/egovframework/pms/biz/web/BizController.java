package egovframework.pms.biz.web;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springmodules.validation.commons.DefaultBeanValidator;

import egovframework.com.cmm.EgovMessageSource;
import egovframework.com.cmm.SessionUtil;
import egovframework.com.cmm.service.EgovFileMngService;
import egovframework.com.cmm.service.EgovFileMngUtil;
import egovframework.com.cmm.service.FileVO;
import egovframework.pms.biz.service.BizModVO;
import egovframework.pms.biz.service.BizService;
import egovframework.pms.biz.service.BizVO;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.service.ComDtlVO;
import egovframework.pms.cmm.web.CommandMap;
import egovframework.rte.fdl.property.EgovPropertyService;
import egovframework.rte.fdl.security.userdetails.util.EgovUserDetailsHelper;
import egovframework.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;

/**
 * 사업관리 
 * @author 공통서비스 개발팀 
 * @see
 *
 */
@Controller
public class BizController {

	/** bizService */
	@Resource(name = "bizService")
	private BizService bizService;

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;

	/** EgovMessageSource */
	@Resource(name = "egovMessageSource")
	EgovMessageSource egovMessageSource;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** DefaultBeanValidator beanValidator */
	@Autowired
	private DefaultBeanValidator beanValidator;

	// 첨부파일 관련
	@Resource(name="EgovFileMngService")
	private EgovFileMngService fileMngService;
	@Resource(name="EgovFileMngUtil")
	private EgovFileMngUtil fileUtil;

	
	private static final Logger logger = LoggerFactory.getLogger(BizController.class);
	
	/**
	 * 사업관리목록을 조회한다. (pageing)
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/biz/bizMngList.do")
	public String bizMngList(@ModelAttribute("bizVO") BizVO bizVO, ModelMap model
    		,HttpSession session, @RequestParam(value = "baseMenuNo", required = false) String baseMenuNo 
			) throws Exception {

		
		
		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated(); 
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		/** EgovPropertyService */
		bizVO.setPageUnit(propertiesService.getInt("pageUnit"));
		bizVO.setPageSize(propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(bizVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(bizVO.getPageUnit());
		paginationInfo.setPageSize(bizVO.getPageSize());

		bizVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		bizVO.setLastIndex(paginationInfo.getLastRecordIndex());
		bizVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		model.addAttribute("resultList", bizService.selectBizList(bizVO));

		int totCnt = bizService.selectBizListTotCnt(bizVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);


		// 선택된 메뉴정보를 세션으로 등록한다.
		if (baseMenuNo != null && !baseMenuNo.equals("") && !baseMenuNo.equals("null")) {
			session.setAttribute("baseMenuNo", baseMenuNo);
		}

		return "pms/biz/bizMngList";
	}
	
	
	
	
	/**
	 * 사업정보 수정을 위해 사업정보를 상세조회한다.
	 * @param bizVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/biz/bizUpdt.do")
	public String bizUpdt(
				@RequestParam("mode") String mode,  
				@ModelAttribute("searchVO") BizModVO bizModVO, ModelMap model) throws Exception {

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

    	model.addAttribute("mode", mode); //수정모드
    	model.addAttribute("bizSeq", bizModVO.getBizSeq()); //수정모드

		
		ComDtlVO vo = new ComDtlVO();
		//공통코드 : 사업구분
		vo.setMstCd("BIZN");
		List<ComDtlVO> lst = cmmService.selectCmmCodeDetail(vo); 
		model.addAttribute("BIZN_result", lst);
		//공통코드 : 공정구분
		vo.setMstCd("PROC");
		model.addAttribute("PROC_result", cmmService.selectCmmCodeDetail(vo));

		
		
		
		
		

		//상세조회
		if("MOD".equals(mode)){
			bizModVO = bizService.selectBiz(bizModVO);
			
		}
		
		bizModVO.setPageIndex(bizModVO.getPageIndex());
		bizModVO.setPageSize(bizModVO.getPageSize());
		bizModVO.setPageUnit(bizModVO.getPageUnit());
		bizModVO.setSearchCondition(bizModVO.getSearchCondition());
		bizModVO.setSearchKeyword(bizModVO.getSearchKeyword());
		//model.addAttribute("bizVO", bizVO);
		model.addAttribute("bizModVO", bizModVO);

		return "pms/biz/bizUpdt";
	}
	
	
	/**
	 * 사업정보 수정후 목록조회 화면으로 이동한다.
	 * @param bizModVO 사업수정정보
	 * @param bindingResult 입력값검증용 bindingResult
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/biz/updateBiz.do")
	public String updateBiz(@ModelAttribute("bizModVO") BizModVO bizModVO, BindingResult bindingResult, Model model) throws Exception {

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		beanValidator.validate(bizModVO, bindingResult);
		if (bindingResult.hasErrors()) {
			
			ComDtlVO vo = new ComDtlVO();
			//공통코드 : 사업구분
			vo.setMstCd("BIZN");
			model.addAttribute("BIZN_result", cmmService.selectCmmCodeDetail(vo));
			//공통코드 : 공정구분
			vo.setMstCd("PROC");
			model.addAttribute("PROC_result", cmmService.selectCmmCodeDetail(vo));
			model.addAttribute("bizSeq", bizModVO.getBizSeq());

			model.addAttribute("resultMsg", "fail.common.update");
			return "pms/biz/bizUpdt";
		} else {

			//마스터 저장처리
			bizService.updateBiz(bizModVO);
			
			//Exception 없이 진행시 수정성공메시지
			model.addAttribute("resultMsg", "success.common.update");
			//return "forward:/pms/biz/bizMngList.do";
			return "forward:/pms/biz/bizUpdt.do";
		}
	}
				
	/**
	 * 사업관리 상세저장처리
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/saveBizDtls.do")
	@ResponseBody
	public ModelAndView saveBizDtls(@RequestBody Map<String,Object> param) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		// 미인증 사업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		JSONObject result = new JSONObject();
    		result.put("result", false);
    		mv.addObject("result", result.toJSONString());
    		return mv;
    	}
		

		// 공정리스트, 팀원리스트  상세를 먼저저장
		String result = bizService.saveSchdList(param);
		
		mv.addObject("result", result);
		return mv;
	}
	
	
	
	/**
	 * 사업마스터 삭제
	 * @param bizModVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/pms/biz/bizDelete.do")
	public String deleteBiz(@ModelAttribute("bizModVO") BizModVO bizModVO, Model model) throws Exception {

		// 미인증 사용자에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

    	Map<String,Object> param = new HashMap<String, Object>();
    	param.put("sqlId", "deleteBiz");
    	Map<String,Object> data = new HashMap<String, Object>();
    	data.put("bizSeq", bizModVO.getBizSeq());
    	param.put("data", data);
    	
		//마스터 저장처리
    	try{
    		cmmService.saveData(param);
    	}catch(Exception e){
    		/// 무결성 저자예외
    		
			ComDtlVO vo = new ComDtlVO();
			//공통코드 : 사업구분
			vo.setMstCd("BIZN");
			model.addAttribute("BIZN_result", cmmService.selectCmmCodeDetail(vo));
			//공통코드 : 공정구분
			vo.setMstCd("PROC");
			model.addAttribute("PROC_result", cmmService.selectCmmCodeDetail(vo));
			model.addAttribute("bizSeq", bizModVO.getBizSeq());

			model.addAttribute("resultMsg", "fail.common.delete.upperMenuExist");
			return "pms/biz/bizUpdt";
}
		
		//Exception 없이 진행시 수정성공메시지
		model.addAttribute("resultMsg", "success.common.delete");
		return "forward:/pms/biz/bizMngList.do";
	}
	
	
	

	/**
	 * 작업목록을 조회한다. (pageing)
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/biz/workMngList.do")
	public String selectWorkList(@ModelAttribute("bizVO") BizModVO bizVO, ModelMap model
    		,HttpSession session, @RequestParam(value = "baseMenuNo", required = false) String baseMenuNo 
			) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

		/** EgovPropertyService */
		bizVO.setPageUnit(propertiesService.getInt("pageUnit"));
		bizVO.setPageSize(propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(bizVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(bizVO.getPageUnit());
		paginationInfo.setPageSize(bizVO.getPageSize());

		bizVO.setFirstIndex(paginationInfo.getFirstRecordIndex());
		bizVO.setLastIndex(paginationInfo.getLastRecordIndex());
		bizVO.setRecordCountPerPage(paginationInfo.getRecordCountPerPage());

		model.addAttribute("resultList", bizService.selectWorkList(bizVO));

		int totCnt = bizService.selectWorkListTotCnt(bizVO);
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);


		// 선택된 메뉴정보를 세션으로 등록한다.
		if (baseMenuNo != null && !baseMenuNo.equals("") && !baseMenuNo.equals("null")) {
			session.setAttribute("baseMenuNo", baseMenuNo);
		}

		return "pms/biz/workMngList";
	}
	
	
	/**
	 * 작업정보 수정을 위해 작업정보를 상세조회한다.
	 * @param bizVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	/*
	@RequestMapping("/pms/biz/workUpdt.do")
	public String workUpdt(
				@RequestParam("mode") String mode,  
				@ModelAttribute("searchVO") BizModVO bizModVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

    	Map<String, Object> map = new HashMap<String, Object>();
    	model.addAttribute("mode", mode); //수정모드

		
		ComDtlVO vo = new ComDtlVO();
		//공통코드 : 사업구분
		vo.setMstCd("BIZN");
		List<ComDtlVO> lst = cmmService.selectCmmCodeDetail(vo); 
		model.addAttribute("BIZN_result", lst);
		//공통코드 : 공정구분
		vo.setMstCd("PROC");
		model.addAttribute("PROC_result", cmmService.selectCmmCodeDetail(vo));



		
		
		//사업명목록
		if("ADD".equals(mode) && (bizModVO.getBizCat() == null || "".equals(bizModVO.getBizCat())) && lst != null){
			map.put("bizCat", lst.get(0).getDtlCd()); //신규등록이면 첫번째 사업구분에 대한 사업리스트 보여줌
		}
		else{
			map.put("bizCat", bizModVO.getBizCat()); //신규등록이면 첫번째 사업구분에 대한 사업리스트 보여줌
		}
		List<BizModVO> lst2 = (List<BizModVO>) cmmService.selectList("selectBizMngList", map);
		model.addAttribute("BIZS_result", lst2);
		
		//공정목록
		if("ADD".equals(mode) && bizModVO.getBizSeq() == -1 && lst2 != null && !lst2.isEmpty()){ //신규일지라도 선택한 사업이 있으면 그사업으로 조회
			map.put("bizSeq", lst2.get(0).getBIZ_SEQ()); //신규등록이면 첫번째 사업에 대한 공정리스트를 보여줌
		}
		else{
    		map.put("bizSeq", bizModVO.getBizSeq()); //사업별 공정조회하기위해
		}
		List<?>  lst3 = cmmService.selectList("selectSchedList", map);
		model.addAttribute("SCHED_result", lst3);
		

		String isBizMngYn = "N"; //해당사업관리자 여부
		//상세조회
		if("MOD".equals(mode)){
			bizModVO = bizService.selectWork(bizModVO); 
    		isBizMngYn = cmmService.isBizMngYn(map);
		}
		
		bizModVO.setPageIndex(bizModVO.getPageIndex());
		bizModVO.setPageSize(bizModVO.getPageSize());
		bizModVO.setPageUnit(bizModVO.getPageUnit());
		bizModVO.setSearchCondition(bizModVO.getSearchCondition());
		bizModVO.setSearchKeyword(bizModVO.getSearchKeyword());

		//model.addAttribute("bizVO", bizVO);
		model.addAttribute("bizModVO", bizModVO);
    	//해당사업관리자 여부
    	model.addAttribute("isBizMngYn", isBizMngYn);



		return "pms/biz/workUpdt";
	}
	 */

	@RequestMapping("/pms/biz/workUpdt.do")
	public String workUpdt(
				@RequestParam("mode") String mode,
				@ModelAttribute("searchVO") BizModVO bizModVO, ModelMap model) throws Exception {
		
		String isBizMngYn = "N"; //해당사업관리자 여부
		
		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
    	
    	model.addAttribute("mode", bizModVO.getMode()); //수정모드

    	if("MOD".equals(bizModVO.getMode())){
    		Map<String, Object> map = new HashMap<String, Object>();
    		
    		// 공정변경마스터조회
    		map.put("bizSeq", bizModVO.getBizSeq());
    		map.put("schedSeq", bizModVO.getSchedSeq());
    		map.put("workSeq", bizModVO.getWorkSeq());	
    		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectWork", map);
    		model.addAttribute("bizDtl", lst.get(0));
	
    		isBizMngYn = cmmService.isBizMngYn(map);
    	}
    	
    	//해당사업관리자 여부
    	model.addAttribute("isBizMngYn", isBizMngYn);
		
    	
		return "pms/biz/workUpdt";
	}
	
	
	
	/**
	 * 작업정보 수정후 목록조회 화면으로 이동한다.
	 * @param bizModVO 작업수정정보
	 * @param bindingResult 입력값검증용 bindingResult
	 * @param model 화면모델
	 * @throws Exception
	 */
	/*
	@RequestMapping("/pms/biz/updateWork.do")
	public String updateWork(
				@RequestParam("mode") String mode,  
				@ModelAttribute("searchVO") BizModVO bizModVO, BindingResult bindingResult, Model model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

    	beanValidator.validate(bizModVO, bindingResult);
    	if (bindingResult.hasErrors()) {
    		
    		//패스워드힌트목록을 코드정보로부터 조회
    		//사업명목록
    		Map<String, Object> map = new HashMap<String, Object>();
    		model.addAttribute("BIZS_result", cmmService.selectList("selectBizMngList", map));
    		
    		ComDtlVO vo = new ComDtlVO();
    		//공통코드 : 사업구분
    		vo.setMstCd("BIZN");
    		model.addAttribute("BIZN_result", cmmService.selectCmmCodeDetail(vo));
    		//공통코드 : 공정구분
    		vo.setMstCd("PROC");
    		model.addAttribute("PROC_result", cmmService.selectCmmCodeDetail(vo));
    		model.addAttribute("bizSeq", bizModVO.getBizSeq());
    		
    		
    		return "pms/biz/workUpdt";
    	} else {
    		bizModVO.setId(SessionUtil.id);
    		//저장처리
    		bizService.updateWork(bizModVO);
    		//Exception 없이 진행시 수정성공메시지
    		model.addAttribute("resultMsg", "success.common.update");
    		return "forward:/pms/biz/workMngList.do";
    	}
	}

			
	
	

	/**
	 * 사업마스터 삭제
	 * @param bizModVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/pms/biz/deleteWork.do")
	public ModelAndView deleteWork(CommandMap commandMap, HttpServletRequest request) throws Exception {

		ModelAndView mv = new ModelAndView("/proc/proc");

    	
		Map<String,Object> param = new HashMap<String, Object>();
    	param.put("sqlId", "deleteWork");
    	Map<String,Object> data = new HashMap<String, Object>();
    	data.put("bizSeq", commandMap.getMap().get("bizSeq"));
    	data.put("schedSeq", commandMap.getMap().get("schedSeq"));
    	data.put("workSeq", commandMap.getMap().get("workSeq"));
    	param.put("data", data);
    	
		//마스터 저장처리
    	try{
    		String result = cmmService.saveData(param);
    		mv.addObject("result", result);
    		
    	}catch(Exception e){
    		/// 무결성 저장예외
    		JSONObject result = new JSONObject();
    		result.put("result", false);
    		result.put("errMsg", "삭제실패");
    		mv.addObject("result", result.toJSONString());
    	}
		
		return mv;
	}
	
		
	
	
	
	
	
	
	

	
////////////////////////////////////////////////////////////// 사업진행
	

	
	/**
	 * 작업목록을 조회한다. (pageing)
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/biz/bizProcList.do")
	public String bizProcList(@ModelAttribute("bizVO") BizModVO bizVO, ModelMap model
			) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

    	Map<String, Object> map = new HashMap<String, Object>();
		/** EgovPropertyService */
    	bizVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	bizVO.setPageSize(propertiesService.getInt("pageSize"));
    	map.put("pageUnit", propertiesService.getInt("pageUnit"));
    	map.put("pageSize", propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(bizVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(bizVO.getPageUnit());
		paginationInfo.setPageSize(bizVO.getPageSize());

    	map.put("firstIndex", paginationInfo.getFirstRecordIndex());
    	map.put("lastIndex", paginationInfo.getLastRecordIndex());
    	map.put("recordCountPerPage", paginationInfo.getRecordCountPerPage());
    	map.put("searchCondition", bizVO.getSearchCondition());
    	map.put("searchKeyword", bizVO.getSearchKeyword());

    	// 조회
    	map.put("bizSeq", bizVO.getBizSeq());
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectProcList", map);
		model.addAttribute("resultList", list);

		// 카운트
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectProcListCnt", map);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		
		
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);



		return "pms/biz/bizProcList";
	}
		
	
	
	/**
	 * 작업정보 수정을 위해 작업정보를 상세조회한다.
	 * @param bizVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/biz/procUpdt.do")
	public String procUpdt(
				@ModelAttribute("searchVO") BizModVO bizModVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
    	
    	model.addAttribute("mode", "MOD"); //수정모드
    	

    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	
    	// 사업마스터조회
    	map.put("bizSeq", bizModVO.getBizSeq());
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectBizDtl", map);
		model.addAttribute("bizDtl", lst.get(0));
		
		// 공정별 작업진행목록
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectSchedProcList", map);
		model.addAttribute("resultList", list);
		
		//해당사업관리자 여부
		String isBizMngYn = cmmService.isBizMngYn(map);
		model.addAttribute("isBizMngYn", isBizMngYn);
    	
		return "pms/biz/procUpdt";
	}

	
	
	
	
	
	
	
	/**
	 * 공정변경목록을 조회한다. (pageing)
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/biz/bizChgList.do")
	public String bizChgList(@ModelAttribute("bizVO") BizModVO bizVO, ModelMap model
			) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

    	Map<String, Object> map = new HashMap<String, Object>();
		/** EgovPropertyService */
    	bizVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	bizVO.setPageSize(propertiesService.getInt("pageSize"));
    	map.put("pageUnit", propertiesService.getInt("pageUnit"));
    	map.put("pageSize", propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(bizVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(bizVO.getPageUnit());
		paginationInfo.setPageSize(bizVO.getPageSize());

    	map.put("firstIndex", paginationInfo.getFirstRecordIndex());
    	map.put("lastIndex", paginationInfo.getLastRecordIndex());
    	map.put("recordCountPerPage", paginationInfo.getRecordCountPerPage());
    	map.put("searchCondition", bizVO.getSearchCondition());
    	map.put("searchKeyword", bizVO.getSearchKeyword());

    	// 조회
    	map.put("bizSeq", bizVO.getBizSeq());
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectChgList", map);
		model.addAttribute("resultList", list);

		// 카운트
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectChgListCnt", map);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		
		
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);


		return "pms/biz/bizChgList";
	}
	
	
	
	
	/**
	 * 공정변경 수정을 위해 작업정보를 상세조회한다.
	 * @param bizVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/biz/chgUpdt.do")
	public String chgUpdt(
				@ModelAttribute("searchVO") BizModVO bizModVO, ModelMap model) throws Exception {
		
		String isBizMngYn = "N"; //해당사업관리자 여부
		
		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
    	
    	model.addAttribute("mode", bizModVO.getMode()); //수정모드

    	if("MOD".equals(bizModVO.getMode())){
    		Map<String, Object> map = new HashMap<String, Object>();
    		
    		// 공정변경마스터조회
    		map.put("bizSeq", bizModVO.getBizSeq());
    		map.put("schedSeq", bizModVO.getSchedSeq());
    		map.put("modiSeq", bizModVO.getModiSeq());	
    		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectChgDtl", map);
    		model.addAttribute("bizDtl", lst.get(0));
    		
    		isBizMngYn = cmmService.isBizMngYn(map);
    	}
    	
    	//해당사업관리자 여부
    	model.addAttribute("isBizMngYn", isBizMngYn);
		
    	
		return "pms/biz/chgUpdt";
	}

	
	
	/**
	 * 단건 폼데이터 저장
	 * @param commandMap
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/saveChgMst.do")
	public ModelAndView saveChgMst(CommandMap commandMap, HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("/proc/proc");

		// 0.공정변경 마스터 채번
		if("ADD".equals(commandMap.getMap().get("mode"))){
			long modiSeq = 0L;
			List<Map<String,Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectModiSeq", commandMap.getMap());
			modiSeq = ((BigDecimal)lst.get(0).get("modiSeq")).intValue();
			commandMap.getMap().put("modiSeq",modiSeq);
		}
		
		
		// 1.공정변경 마스터 저장
		String result = bizService.saveChgMst(commandMap.getMap());
		mv.addObject("result", result);

		return mv;
	}

	
	
	
	



	
	/**
	 * 산출물목록을 조회한다. (pageing)
	 * @param bizVO 검색조건정보
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping(value = "/pms/biz/docMngList.do")
	public String docMngList(@ModelAttribute("bizVO") BizModVO bizVO, ModelMap model
			) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

    	Map<String, Object> map = new HashMap<String, Object>();
		/** EgovPropertyService */
    	bizVO.setPageUnit(propertiesService.getInt("pageUnit"));
    	bizVO.setPageSize(propertiesService.getInt("pageSize"));
    	map.put("pageUnit", propertiesService.getInt("pageUnit"));
    	map.put("pageSize", propertiesService.getInt("pageSize"));

		/** pageing */
		PaginationInfo paginationInfo = new PaginationInfo();
		paginationInfo.setCurrentPageNo(bizVO.getPageIndex());
		paginationInfo.setRecordCountPerPage(bizVO.getPageUnit());
		paginationInfo.setPageSize(bizVO.getPageSize());

    	map.put("firstIndex", paginationInfo.getFirstRecordIndex());
    	map.put("lastIndex", paginationInfo.getLastRecordIndex());
    	map.put("recordCountPerPage", paginationInfo.getRecordCountPerPage());
    	map.put("searchCondition", bizVO.getSearchCondition());
    	map.put("searchKeyword", bizVO.getSearchKeyword());

    	// 조회
    	map.put("bizSeq", bizVO.getBizSeq());
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectDocMngList", map);
		model.addAttribute("resultList", list);

		// 카운트
		List<Map<String, Object>> listCnt = (List<Map<String, Object>>) cmmService.selectList("selectDocMngListCnt", map);
		int totCnt = 0;
		try{
			totCnt = ((BigDecimal) listCnt.get(0).get("totCnt")).intValue();
		}catch(Exception e){
			logger.debug("..." + e.getMessage());
		};
		
		
		paginationInfo.setTotalRecordCount(totCnt);
		model.addAttribute("paginationInfo", paginationInfo);
		model.addAttribute("bizVO", bizVO);



		return "pms/biz/docMngList";
	}
		
	
	
	/**
	 * 작업정보 수정을 위해 작업정보를 상세조회한다.
	 * @param bizVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/biz/docUpdt.do")
	public String docUpdt(
				@ModelAttribute("searchVO") BizModVO bizModVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}
    	
    	model.addAttribute("mode", "MOD"); //수정모드
    	

    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	
    	// 사업마스터조회
    	map.put("bizSeq", bizModVO.getBizSeq());
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectBizDtl", map);
		model.addAttribute("bizDtl", lst.get(0));
		// 첨부파일정보
		bizModVO.setFileSeq((String) lst.get(0).get("fileSeq"));
		model.addAttribute("bizModVO", bizModVO);
		
		// 공정별 작업진행목록
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectSchedProcList", map);
		model.addAttribute("resultList", list);
		
		
		//해당사업관리자 여부
		model.addAttribute("isBizMngYn", cmmService.isBizMngYn(map));
		
		return "pms/biz/docUpdt";
	}

	
	
	/**
	 * 산출물파일 저장후 상세조회한다.
	 * @param eduVO 검색조건
	 * @param model 화면모델
	 * @throws Exception
	 */
	@RequestMapping("/pms/biz/docFileUpdate.do")
	public String eduDataUpdate(
				final MultipartHttpServletRequest multiRequest,
				@RequestParam("mode") String mode,  
				@ModelAttribute("bizModVO") BizModVO bizModVO, ModelMap model) throws Exception {

		// 미인증 작업에 대한 보안처리
		Boolean isAuthenticated = EgovUserDetailsHelper.isAuthenticated();
    	if(!isAuthenticated) {
    		model.addAttribute("message", egovMessageSource.getMessage("fail.common.login"));
        	return "uat/uia/EgovLoginUsr";
    	}

        if(mode.equals("ADD") || mode.equals("UPDATE") ){
        	
        }
        	
        	
    // 첨부파일 관련 첨부파일ID 생성
		List<FileVO> _result = null;
		String _fileSeq = bizModVO.getFileSeq();

		final Map<String, MultipartFile> files = multiRequest.getFileMap();

		if(!files.isEmpty()){
    		 // 파일전송 
    		 _result = fileUtil.parseFileInf(files, "DSCH_", 0, _fileSeq, "DATA");
    		 if( _fileSeq == "" || _fileSeq.isEmpty() || _fileSeq == null ){
	    		 // 키 생성 및 첨부테이블 등록 처리 
	    		 _fileSeq = fileMngService.insertFileInfs(_result);  //파일이 생성되고나면 생성된 첨부파일 ID를 리턴한다.
    		 } else {
    			// 첨부테이블 수정 처리 
	    		fileMngService.updateFileInfs(_result);  //파일이 생성되고나면 생성된 첨부파일 ID를 리턴한다.
    		 }
		}
		
		bizModVO.setRegId(SessionUtil.id);
		bizModVO.setFileSeq(_fileSeq);
    	
		// 저장 처리 
    	//eduDataService.updateEdu(bizModVO); 
    	Map<String,Object> param = new HashMap<String, Object>();
    	param.put("sqlId", "updateBizFile");
    	Map<String,Object> data = new HashMap<String, Object>();
    	data.put("bizSeq", bizModVO.getBizSeq());
    	data.put("fileSeq", _fileSeq);
    	param.put("data", data);
    	
		cmmService.saveData(param);
		
		
		
		
		// 자기화면으로 리턴..
		// 사업마스터조회
		List<Map<String, Object>> lst = (List<Map<String, Object>>) cmmService.selectList("selectBizDtl", data);
		model.addAttribute("bizDtl", lst.get(0));
		// 첨부파일정보
		bizModVO.setFileSeq((String) lst.get(0).get("fileSeq"));
		model.addAttribute("bizModVO", bizModVO);
		
		// 공정별 작업진행목록
		List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectSchedProcList", data);
		model.addAttribute("resultList", list);
        
		//model.addAttribute("message", egovMessageSource.getMessage("success.common.insert"));
		model.addAttribute("resultMsg", "success.common.insert");
		
		return "pms/biz/docUpdt";

	}	
	

	
}
