package egovframework.let.main.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import egovframework.com.cmm.ComDefaultVO;
import egovframework.com.cmm.LoginVO;
import egovframework.pms.cmm.service.CmmService;
import egovframework.pms.cmm.service.MenuManageVO;
import egovframework.rte.fdl.security.userdetails.util.EgovUserDetailsHelper;

/**
 * 템플릿 메인 페이지 컨트롤러 클래스(Sample 소스)
 * @author 실행환경 개발팀 JJY
 * @since 2011.08.31
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2011.08.31  JJY            최초 생성
 *
 * </pre>
 */
@Controller@SessionAttributes(types = ComDefaultVO.class)
public class EgovMainController {

	/** cmmService */
	@Resource(name = "cmmService")
	private CmmService cmmService;

	/**
	 * 템플릿 메인 페이지 조회
	 * @return 메인페이지 정보 Map [key : 항목명]
	 *
	 * @param request
	 * @param model
	 * @exception Exception Exception
	 */
	@RequestMapping(value = "/cmm/main/mainPage.do")
	public String getMgtMainPage(HttpServletRequest request, ModelMap model)
	  throws Exception{

		

		// 업무게시판 메인컨텐츠 조회 끝 -----------------------------------

		return "main/EgovMainView";
	}

	/**
     * Head메뉴를 조회한다.
     * @param menuManageVO MenuManageVO
     * @return 출력페이지정보 "main_headG", "main_head"
     * @exception Exception
     */
    @RequestMapping(value="/sym/mms/EgovMainMenuHead.do")
    public String selectMainMenuHead(
    		@ModelAttribute("menuManageVO") MenuManageVO menuManageVO
    		,ModelMap model
    		) throws Exception {
    	
    	// 스프링시큐리티 - 사용자인증정보
    	LoginVO user = EgovUserDetailsHelper.isAuthenticated()? (LoginVO)EgovUserDetailsHelper.getAuthenticatedUser():null;

		if(EgovUserDetailsHelper.isAuthenticated() && user!=null){
			menuManageVO.setTmp_Id(user.getUserId());
			menuManageVO.setTmp_Password(user.getUserPwd());
			menuManageVO.setTmp_Name(user.getUserNm());
    	}
		menuManageVO.setUpperMenuNo(0);
		
		Map<String, Object> map = new HashMap<String, Object>();
		List<?> list = cmmService.selectList("selectMainMenuHead", map);
		model.addAttribute("list_headmenu", list);
		//model.addAttribute("list_menulist", menuManageService.selectMainMenuLeft(menuManageVO));

		return "main/inc/EgovIncTopnav"; // 내부업무의 상단메뉴 화면
    }


    /**
     * 좌측메뉴를 조회한다.
     * @param menuManageVO MenuManageVO
     * @param vStartP      String
     * @return 출력페이지정보 "main_left"
     * @exception Exception
     */
    @RequestMapping(value="/sym/mms/EgovMainMenuLeft.do")
    public String selectMainMenuLeft(
    		@ModelAttribute("menuManageVO") MenuManageVO menuManageVO,ModelMap model
			)
            throws Exception {

    	//menuManageVO.setUpperMenuNo(upperMenuNo);
    	
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("upperMenuNo", menuManageVO.getUpperMenuNo());
    	List<Map<String, Object>> list = (List<Map<String, Object>>) cmmService.selectList("selectMainMenuLeft", map);
		model.addAttribute("list_menulist", list);

		model.addAttribute("resultList", list);
		if(list != null && list.size()>0){
			model.addAttribute("upperMenuNm", list.get(0).get("upperMenuNm"));
			model.addAttribute("upperMenuNo", list.get(0).get("upperMenuNo"));
		}

		
      	return "main/inc/EgovIncLeftmenu";
    }

}